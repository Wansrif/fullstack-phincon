// CALCULATOR

// GET NUMBER
const getNumber = (num) => {
  let result = document.getElementById("result");
  if (result.value === "0") {
    result.value = "";
    result.value += num;
  } else {
    result.value += num;
  }
};

// CLEAR RESULT
const clearResult = () => {
  let result = document.getElementById("result");
  result.value = "0";
}

// DELETE NUMBER
const delNumber = () => {
  let result = document.getElementById("result");
  if (result.value.length == 1) {
    result.value = "0";
  } else {
    result.value = result.value.slice(0, -1);
  }
}

// PLUS NUMBER
const plusNumber = () => {
  let result = document.getElementById("result");
  if (result.value === "0") {
    result.value = "";
  } else if (result.value.slice(-1) === "+") {
    result.value = result.value.slice(0, -1);
    result.value += "+";
  } else {
    result.value += "+";
  }
}

// MINUS NUMBER
const minusNumber = () => {
  let result = document.getElementById("result");
  if (result.value === "0") {
    result.value = "";
  } else if (result.value.slice(-1) === "-") {
    result.value = result.value.slice(0, -1);
    result.value += "-";
  } else {
    result.value += "-";
  }
}

// MULTIPLY NUMBER
const multiplyNumber = () => {
  let result = document.getElementById("result");
  if (result.value === "0") {
    result.value = "";
  } else if (result.value.slice(-1) === "*") {
    result.value = result.value.slice(0, -1);
    result.value += "*";
  } else {
    result.value += "*";
  }
}

// DIVIDE NUMBER
const divideNumber = () => {
  let result = document.getElementById("result");
  if (result.value === "0") {
    result.value = "";
  } else if (result.value.slice(-1) === "/") {
    result.value = result.value.slice(0, -1);
    result.value += "/";
  } else {
    result.value += "/";
  }
}

// POINT NUMBER
const pointNumber = () => {
  let result = document.getElementById("result");
  if (result.value === "0") {
    result.value = "";
  } else if (result.value.slice(-1) === ".") {
    result.value = result.value.slice(0, -1);
    result.value += ".";
  } else {
    result.value += ".";
  }
}

// EQUALS NUMBER
const equalNumber = () => {
  let result = document.getElementById("result").value;
  if (result === "") {
    result = "0";
  } else {
    if (
      result.slice(-1) === "." ||
      result.slice(-1) === "+" ||
      result.slice(-1) === "-" ||
      result.slice(-1) === "*" ||
      result.slice(-1) === "/"
    ) {
      console.log("syntaxError");
      result = "syntaxError";
    } else {
      try {
        const evaluatedResult = eval(result);
        if (!isFinite(evaluatedResult)) {
          console.log("syntaxError");
          result = "syntaxError";
        } else {
          result = evaluatedResult;
        }
      } catch (error) {
        console.log("syntaxError");
        result = "syntaxError";
      }
    }
  }
  document.getElementById("result").value = result;
}
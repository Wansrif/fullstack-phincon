import fs from 'fs';
import { getMyPokemons, addPokemon, renamePokemon } from '../../models/myPokemonModel';

jest.mock('fs', () => ({
  existsSync: jest.fn(),
  readFileSync: jest.fn(),
  writeFileSync: jest.fn(),
}));

describe('myPokemonModel', () => {
  afterEach(() => {
    fs.existsSync.mockReset();
    fs.readFileSync.mockReset();
    fs.writeFileSync.mockReset();
  });

  describe('getMyPokemons', () => {
    it('should return an array of saved pokemons', () => {
      fs.existsSync.mockReturnValue(true);
      fs.readFileSync.mockReturnValue(JSON.stringify({ myPokemons: ['somePokemon'] }));

      const result = getMyPokemons();

      expect(result).toEqual(['somePokemon']);
    });

    it('should handle file not found gracefully', () => {
      fs.existsSync.mockReturnValue(false);

      const result = getMyPokemons();

      expect(result).toEqual([]);
    });

    it('should handle read error gracefully', () => {
      fs.existsSync.mockReturnValue(true);
      fs.readFileSync.mockImplementation(() => {
        throw new Error('Read error');
      });

      const result = getMyPokemons();

      expect(result).toEqual([]);
    });
  });

  describe('addPokemon', () => {
    it('should add a new pokemon', () => {
      const writeFileSyncMock = jest.spyOn(fs, 'writeFileSync');
      fs.existsSync.mockReturnValue(true);
      fs.readFileSync.mockReturnValue(JSON.stringify({ myPokemons: ['existingPokemon'] }));

      addPokemon('newPokemon');

      expect(writeFileSyncMock).toHaveBeenCalledWith(
        './database/db.json',
        JSON.stringify({ myPokemons: ['existingPokemon', 'newPokemon'] }, null, 2),
        'utf-8'
      );
    });
  });

  describe('renamePokemon', () => {
    it('should rename a pokemon', () => {
      const writeFileSyncMock = jest.spyOn(fs, 'writeFileSync');
      fs.existsSync.mockReturnValue(true);
      fs.readFileSync.mockReturnValue(
        JSON.stringify({ myPokemons: [{ id: 1, name: 'Pikachu', release: '2023-08-01' }] })
      );

      renamePokemon(1, 'Raichu', '2023-08-15');

      expect(writeFileSyncMock).toHaveBeenCalledWith(
        './database/db.json',
        JSON.stringify(
          {
            myPokemons: [{ id: 1, name: 'Raichu', release: '2023-08-15' }],
          },
          null,
          2
        ),
        'utf-8'
      );
    });
  });
});

import axios from 'axios';
import pokemonController from '../../controllers/pokemonController';
import { addPokemon, getMyPokemons, renamePokemon } from '../../models/myPokemonModel';
import responseHelper from '../../helpers/responseHelper';

jest.mock('axios');
jest.mock('uid', () => ({
  uid: jest.fn().mockReturnValue('someId'),
}));
jest.mock('../../models/myPokemonModel');
jest.mock('../../helpers/responseHelper');

describe('pokemonController', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    req = { params: {} };
    res = {
      status: jest.fn(() => res),
      json: jest.fn(),
    };
    next = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('getPokemons', () => {
    it('should handle successful getPokemons', async () => {
      axios.get.mockResolvedValue({ data: 'someData' });

      await pokemonController.getPokemons(req, res);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/');
      expect(responseHelper).toHaveBeenCalledWith(res, 200, 'someData', 'Success');
    });

    it('should handle error in getPokemons', async () => {
      axios.get.mockRejectedValue(new Error('Some error'));

      await pokemonController.getPokemons(req, res);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/');
      expect(responseHelper).toHaveBeenCalledWith(res, 500, null, 'Some error');
    });
  });

  describe('getPokemonByName', () => {
    it('should handle successful getPokemonByName', async () => {
      axios.get.mockResolvedValue({ data: 'someData' });
      req.params = { name: 'bulbasaur' };

      await pokemonController.getPokemonByName(req, res);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/bulbasaur');
      expect(responseHelper).toHaveBeenCalledWith(res, 200, 'someData', 'Success');
    });

    it('should handle error in getPokemonByName', async () => {
      axios.get.mockRejectedValue({ response: { status: 400, data: 'Bad Request' } });
      req.params = { name: 'bulbasaur' };

      await pokemonController.getPokemonByName(req, res);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/bulbasaur');
      expect(responseHelper).toHaveBeenCalledWith(res, 400, null, 'Bad Request');
    });
  });

  describe('catchPokemon', () => {
    it('should handle successful catchPokemon', async () => {
      const data = { id: 'someId', name: 'bulbasaur', release: 0 };
      addPokemon.mockReturnValue(data);

      req.body = { name: 'bulbasaur' };
      Math.random = jest.fn(() => 0.6);

      await pokemonController.catchPokemon(req, res);

      expect(addPokemon).toHaveBeenCalledWith(data);
      expect(responseHelper).toHaveBeenCalledWith(res, 200, data, 'Nice, you catch the pokemon!');
    });

    it('should handle unsuccessful catchPokemon', async () => {
      req.body = { name: 'bulbasaur' };
      Math.random = jest.fn(() => 0.4); // Mock Math.random to return a value < 0.5

      await pokemonController.catchPokemon(req, res);

      expect(addPokemon).not.toHaveBeenCalled(); // Make sure addPokemon is not called
      expect(responseHelper).toHaveBeenCalledWith(res, 400, null, 'Oops, the pokemon run away...');
    });

    it('should handle error in catchPokemon', async () => {
      addPokemon.mockImplementation(() => {
        throw new Error('Some error');
      });

      req.body = { name: 'bulbasaur' };
      Math.random = jest.fn(() => 0.6);

      await pokemonController.catchPokemon(req, res);

      expect(addPokemon).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500, null, 'Some error');
    });
  });

  describe('getMyPokemons', () => {
    it('should handle successful getMyPokemons', async () => {
      const data = [{ id: 'someId', name: 'bulbasaur', release: 0 }];
      getMyPokemons.mockReturnValue(data);

      await pokemonController.getMyPokemons(req, res);

      expect(getMyPokemons).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 200, data, 'Success');
    });

    it('should handle error in getMyPokemons', async () => {
      getMyPokemons.mockImplementation(() => {
        throw new Error('Some error');
      });

      await pokemonController.getMyPokemons(req, res);

      expect(getMyPokemons).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500, null, 'Some error');
    });
  });

  describe('releasePokemon', () => {
    it('should release the Pokemon if it exists and random number is prime', async () => {
      // Arrange
      req.params.id = 'someId';
      getMyPokemons.mockReturnValue([{ id: 'someId' }]);
      axios.get.mockResolvedValue({});

      // Act
      await pokemonController.releasePokemon(req, res, next);

      // Assert
      expect(next).toHaveBeenCalled();
    });

    it('should respond with 404 if the Pokemon does not exist', async () => {
      // Arrange
      req.params.id = 'nonExistentId';
      getMyPokemons.mockReturnValue([]);

      // Act
      await pokemonController.releasePokemon(req, res, next);

      // Assert
      expect(responseHelper).toHaveBeenCalledWith(res, 404, null, 'Pokemon not found');
    });

    // it('should respond with 400 if the random number is not prime', async () => {
    //   // Arrange
    //   req.params.id = 'someId';
    //   getMyPokemons.mockReturnValue([{ id: 'someId' }]);
    //   axios.get.mockResolvedValue({});
    //   pokemonController.isPrime = jest.fn(() => false);

    //   // Act
    //   await pokemonController.releasePokemon(req, res, next);

    //   // Assert
    //   expect(responseHelper).toHaveBeenCalledWith(res, 400, null, 'Failed to release pokemon');
    // });

    it('should respond with 500 if an error occurs while getting data', async () => {
      // Arrange
      req.params.id = 'someId';
      getMyPokemons.mockImplementation(() => {
        throw new Error('Mocked error');
      });

      // Act
      await pokemonController.releasePokemon(req, res, next);

      // Assert
      expect(responseHelper).toHaveBeenCalledWith(res, 500, null, 'Mocked error');
    });
  });

  describe('evolvePokemon', () => {
    it('should evolve the pokemon and return success message', async () => {
      // Arrange
      const id = 'someId';
      const pokemon = { id, name: 'bulbasaur', release: 0 };
      getMyPokemons.mockReturnValue([pokemon]);

      // Act
      await pokemonController.evolvePokemon({ params: { id } }, res);

      // Assert
      expect(getMyPokemons).toHaveBeenCalled();
      expect(renamePokemon).toHaveBeenCalledWith(id, 'bulbasaur-0', 1);
      expect(responseHelper).toHaveBeenCalledWith(res, 200, null, 'Congrats, your pokemon has been evolved!');
    });

    it('should handle the case when the pokemon name has two parts', async () => {
      // Arrange
      const id = 'someId';
      const pokemon = { id, name: 'charizard-mega', release: 2 };
      getMyPokemons.mockReturnValue([pokemon]);

      // Act
      await pokemonController.evolvePokemon({ params: { id } }, res);

      // Assert
      expect(getMyPokemons).toHaveBeenCalled();
      expect(renamePokemon).toHaveBeenCalledWith(id, 'charizard-2', 3);
      expect(responseHelper).toHaveBeenCalledWith(res, 200, null, 'Congrats, your pokemon has been evolved!');
    });

    it('should handle error and return 500 status', async () => {
      // Arrange
      const id = 'someId';
      const errorMessage = 'Some error occurred';
      getMyPokemons.mockImplementation(() => {
        throw new Error(errorMessage);
      });

      // Act
      await pokemonController.evolvePokemon({ params: { id } }, res);

      // Assert
      expect(getMyPokemons).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500, null, errorMessage);
    });
  });
});

import express from 'express';
import pokemonController from '../controllers/pokemonController.js';

const pokemonRouter = express.Router();

pokemonRouter.get('/', pokemonController.getPokemons);
pokemonRouter.get('/detail/:name', pokemonController.getPokemonByName);
pokemonRouter.post('/catch-pokemon', pokemonController.catchPokemon);
pokemonRouter.get('/my-pokemon', pokemonController.getMyPokemons);
pokemonRouter.put('/evolution/:id', pokemonController.releasePokemon, pokemonController.evolvePokemon);

export default pokemonRouter;

import axios from 'axios';
import { uid } from 'uid';
import responseHelper from '../helpers/responseHelper.js';
import { addPokemon, getMyPokemons, renamePokemon } from '../models/myPokemonModel.js';

const baseUrl = 'https://pokeapi.co/api/v2/pokemon/';

// Check prime number
const isPrime = (num) => {
  for (let i = 2; i < num; i++) if (num % i === 0) return false;
  return num > 1;
};

const pokemonController = {
  getPokemons: async (req, res) => {
    try {
      const response = await axios.get(baseUrl);
      const { data } = response;
      responseHelper(res, 200, data, 'Success');
    } catch (error) {
      responseHelper(res, 500, null, error.message);
    }
  },

  getPokemonByName: async (req, res) => {
    try {
      const { name } = req.params;
      const response = await axios.get(`${baseUrl}${name}`);
      const { data } = response;
      responseHelper(res, 200, data, 'Success');
    } catch (error) {
      if (error.response) {
        responseHelper(res, error.response.status, null, error.response.data);
      } else {
        responseHelper(res, 500, null, error.message);
      }
    }
  },

  catchPokemon: async (req, res) => {
    try {
      const { name } = req.body;

      const random = Math.random() >= 0.5;

      if (random) {
        const data = {
          id: uid(),
          name,
          release: 0,
        };
        addPokemon(data);
        responseHelper(res, 200, data, 'Nice, you catch the pokemon!');
      } else {
        responseHelper(res, 400, null, 'Oops, the pokemon run away...');
      }
    } catch (error) {
      responseHelper(res, 500, null, error.message);
    }
  },

  getMyPokemons: async (req, res) => {
    try {
      const data = getMyPokemons();
      responseHelper(res, 200, data, 'Success');
    } catch (error) {
      responseHelper(res, 500, null, error.message);
    }
  },

  releasePokemon: async (req, res, next) => {
    try {
      const { id } = req.params;
      const data = getMyPokemons();
      const pokemon = data.find((item) => item.id === id);
      if (!pokemon) {
        responseHelper(res, 404, null, 'Pokemon not found');
      } else {
        const random = Math.floor(Math.random() * 100) + 1;
        const prime = isPrime(random);

        if (prime) {
          next();
        } else {
          responseHelper(res, 400, null, 'Failed to release pokemon');
        }
      }
    } catch (error) {
      responseHelper(res, 500, null, error.message);
    }
  },

  evolvePokemon: async (req, res) => {
    try {
      const { id } = req.params;
      const data = getMyPokemons();
      const pokemon = data.find((item) => item.id === id);
      const name = pokemon.name.split('-');

      const fibonacci = (num) => {
        if (num <= 0) return 0;
        if (num === 1) return 1;
        return fibonacci(num - 1) + fibonacci(num - 2);
      };

      const newRelease = pokemon.release + 1;
      const newName = `${name[0]}-${name[1] ? fibonacci(newRelease) : 0}`;

      renamePokemon(id, newName, newRelease);
      responseHelper(res, 200, null, 'Congrats, your pokemon has been evolved!');
    } catch (error) {
      responseHelper(res, 500, null, error.message);
    }
  },
};

export default { ...pokemonController, isPrime };

import express from 'express';
import pokemonRouter from './routes/pokemonRouter.js';

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/pokemon', pokemonRouter);

app.use((req, res, next) => {
  const error = new Error('API not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Pokemon app listening at http://localhost:${port}`);
});

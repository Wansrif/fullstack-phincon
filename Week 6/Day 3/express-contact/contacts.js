import fs from "fs";

// Create a data folder if it doesn't already exist
const dirPath = "./data";
if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath);
}

// Create a contacts.json file if it does not already exist with the initial structure
const dataPath = "./data/contacts.json";
if (!fs.existsSync(dataPath)) {
  const initialData = {
    data: [],
  };
  fs.writeFileSync(dataPath, JSON.stringify(initialData), "utf-8");
}

export const loadContact = () => {
  try {
    const data = fs.readFileSync(dataPath, "utf8");
    return JSON.parse(data);
  } catch (error) {
    return [];
  }
};

const writeContacts = (contacts) => {
  fs.writeFileSync(dataPath, JSON.stringify({ data: contacts }, null, 2));
};

export const findContact = (nama) => {
  const contacts = loadContact();
  const contact = contacts.data.find((contact) => contact.nama.toLowerCase() === nama.toLowerCase());
  return contact;
};

export const addContact = (contact) => {
  const contacts = loadContact();
  const duplicateName = contacts.data.find((c) => c.nama === contact.nama);
  if (duplicateName) {
    throw new Error("Contact name already registered");
  }
  const duplicateEmail = contacts.data.find((c) => c.email === contact.email);
  if (duplicateEmail) {
    throw new Error("Email already registered");
  }
  contacts.data.push(contact);
  writeContacts(contacts.data);
};

export const deleteContact = (nama) => {
  const contacts = loadContact();
  const filteredContacts = contacts.data.filter((contact) => contact.nama.toLowerCase() !== nama.toLowerCase());
  writeContacts(filteredContacts);
  return filteredContacts.length !== contacts.data.length;
};

export const updateContact = (name, updatedContact) => {
  const contacts = loadContact();
  const index = contacts.data.findIndex((contact) => contact.nama === name);
  if (index !== -1) {
    contacts.data[index] = updatedContact;
    writeContacts(contacts.data);
    return true;
  }
  return false;
};

export default {
  loadContact,
  findContact,
  addContact,
  deleteContact,
  updateContact,
};

import express from "express";
import contacts from "./contacts.js";

const app = express();
const port = 3000;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/contacts", (req, res) => {
  try {
    res.send(contacts.loadContact());
    res.status(200);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.get("/contacts/:nama", (req, res) => {
  try {
    const contact = contacts.findContact(req.params.nama);
    if (contact) {
      res.status(200).json(contact);
    } else {
      res.status(404).json({ message: "Contact not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.post("/contacts", (req, res) => {
  try {
    contacts.addContact(req.body);
    res.status(201).json(req.body);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.delete("/contacts/:nama", (req, res) => {
  try {
    const contactDeleted = contacts.deleteContact(req.params.nama);
    if (contactDeleted) {
      res.status(200).json({ message: "Contact data successfully deleted" });
    } else {
      res.status(404).json({ message: "Contact not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.put("/contacts/:nama", (req, res) => {
  try {
    const nameToUpdate = req.params.nama;
    const updatedContact = req.body;

    if (contacts.updateContact(nameToUpdate, updatedContact)) {
      res.status(200).json({ message: "Contact data is successfully changed" });
    } else {
      res.status(404).json({ message: "Contact not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.listen(port, () => {
  console.log(`Express app listening at http://localhost:${port}`);
});

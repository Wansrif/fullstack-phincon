// 1. Write a function that does the following for the given values: add, subtract, divide and multiply. These are simply referred to as the basic arithmetic operations. The variables have to be defined, but in this challenge they will be defined for you. All you have to do is check the variables, do some string to integer conversions, use some if conditions, and apply the arithmetic operation.
// The numbers and operation are given as strings and should result in an integer value.

// Note:
// The numbers and operation are given as strings and should result in an integer value.
// If the operation results in Infinity, then return "undefined" (e.g. division by zero).
// Division results will be rounded down to their integral parts.

// Example:
// operation("1",  "2",  "add" ) ➞ 3
// operation("4",  "5",  "subtract") ➞ -1
// operation("6",  "3",  "divide") ➞ 2
// operation("2",  "7",  "multiply") ➞ 14
// operation("6",  "0",  "divide") ➞ ‘undefined’

const operation = (num1, num2, operation) => {
  let result = 0;
  if (operation === 'add') {
    result = parseInt(num1) + parseInt(num2);
  } else if (operation === 'subtract') {
    result = parseInt(num1) - parseInt(num2);
  } else if (operation === 'divide') {
    if (parseInt(num2) === 0) {
      return 'undefined';
    }
    result = parseInt(num1) / parseInt(num2);
  } else if (operation === 'multiply') {
    result = parseInt(num1) * parseInt(num2);
  }
  return result;
}

// console.log(operation("1", "2", "add"));
// console.log(operation("4", "5", "subtract"));
// console.log(operation("6", "3", "divide"));
// console.log(operation("2", "7", "multiply"));
// console.log(operation("6", "0", "divide"));



// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// 2. Create a function that takes an array of strings and returns an array with only the strings that have numbers in them. If there are no strings containing numbers, return an empty array.

// Example:
// numInStr(["1a", "a", "2b", "b"]) ➞ ["1a", "2b"]
// numInStr(["abc", "abc10"]) ➞ ["abc10"]
// numInStr(["abc", "ab10c", "a10bc", "bcd"]) ➞ ["ab10c", "a10bc"]
// numInStr(["this is a test", "test1"]) ➞ ["test1"]
// Notes
// The strings can contain white spaces or any type of characters.

const numInStr = (arr) => {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    let str = arr[i];
    // console.log(str)
    for (let j = 0; j < str.length; j++) {
      if (parseInt(str[j])) {
        // console.log(parseInt(str[j]))
        // console.log(str)
        result.push(str);
        break;
      }
    }
  }
  return result;
}

// console.log(numInStr(["1a", "a", "2b", "b"]));
// console.log(numInStr(["abc", "abc10"]));
// console.log(numInStr(["abc", "ab10c", "a10bc", "bcd"]));
// console.log(numInStr(["this is a test", "test1"]));



// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// 3. We can assign a value to each character in a word, based on their position in the alphabet (a = 1, b = 2, ... , z = 26). A balanced word is one where the sum of values on the left-hand side of the word equals the sum of values on the right-hand side. For odd length words, the middle character (balance point) is ignored.
// Write a function that returns true if the word is balanced, and false if it's not.

// Example
// balanced("zips") ➞ true
// "zips" = "zi|ps" = 26+9|16+19 = 35|35 = true
// balanced("brake") ➞ false
// "brake" = "br|ke" = 2+18|11+5 = 20|16 = false

const balanced = (str) => {
  const alphaVal = (s) => s.toLowerCase().charCodeAt(0) - 97 + 1;
  let left = 0;
  let right = 0;
  for (let i = 0; i < str.length / 2; i++) {
    left += alphaVal(str[i]);
  }
  for (let i = Math.ceil(str.length / 2); i < str.length; i++) {
    right += alphaVal(str[i]);
  }
  if (left === right) {
    console.log(str + '-' + true)
  } else {
    console.log(str + '-' + false)
  }
}

balanced('zips') /* true */
balanced('brake') /* false */
balanced('at') /* false */
balanced('forgetful') /* false */
balanced('vegetation') /* true */


// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// 4. 2048 is a game where you need to slide numbered tiles (natural powers of 2) up, down, left or right on a square grid to combine them in a tile with the number 2048.
// The sliding procedure is described by the following rules:
// Tiles slide as far as possible in the chosen direction until they are stopped by either another tile or the edge of the grid.
// If two tiles of the same number collide while moving, they will merge into a tile with the total value of the two tiles that collided.
// If more than one variant of merging is possible, move direction shows one that will take effect.
// Tile cannot merge with another tile more than one time.
// Sliding is done almost the same for each direction and for each row/column of the grid, so your task is to implement only the left slide for a single row.

// Note
// Input row can be of any size (empty too).
// Input row will contain only natural powers of 2 and 0 for empty tiles.
// Keep trailing zeros in the output.

// Example
// leftSlide([2, 2, 2, 0]) ➞ [4, 2, 0, 0] // Merge left-most tiles first.
// leftSlide([2, 2, 4, 4, 8, 8]) ➞ [4, 8, 16, 0, 0, 0] // Only merge once.

const leftSlide = (slide) => {
  const sort = slide.sort((a, b) => {
    if (b === 0) {
      return -1
    }
  })
  // console.log(sort)
  for (x = 0; x < sort.length; x++) {
    if (sort[x] === sort[x + 1]) {
      sort[x] += sort[x + 1]
      sort[x + 1] = 0
    }
  }
  const sort2 = slide.sort((a, b) => {
    if (b === 0) {
      return -1
    }
  })
  console.log(sort2)
}

// leftSlide([2, 2, 2, 0]) /* Expected ➞ [4, 2, 0, 0] */
// leftSlide([2, 2, 4, 4, 8, 8]) /* Expected ➞ [4, 8, 16, 0, 0, 0] */
// leftSlide([0, 2, 0, 2, 4]) /* Expected ➞ [4, 4, 0, 0, 0] */
// leftSlide([0, 2, 2, 8, 8, 8]) /* Expected ➞ [4, 16, 8, 0, 0, 0] */
// leftSlide([0, 0, 0, 0]) /* Expected ➞ [0, 0, 0, 0] */
// leftSlide([0, 0, 0, 2]) /* Expected ➞ [2, 0, 0, 0] */
// leftSlide([2, 0, 0, 0]) /* Expected ➞ [2, 0, 0, 0] */
// leftSlide([8, 2, 2, 4]) /* Expected ➞ [8, 4, 4, 0] */
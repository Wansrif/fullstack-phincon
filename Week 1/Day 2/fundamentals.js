// JSCHALLENGER https://www.jschallenger.com/javascript-practice

// JAVASCRIPT FUNDAMENTALS

// Sum two numbers
// Write a function that takes two numbers (a and b) as argument. Sum a and b. Return the result
function myFunction(a, b) {
  return a + b
}

// Comparison operators, strict equality
// Write a function that takes two values, say a and b, as arguments. Return true if the two values are equal and of the same type
function myFunction(a, b) {
  return a === b
}

// Get type of value
// Write a function that takes a value as argument. Return the type of the value.
function myFunction(a) {
  return typeof a;
}

// Get nth character of string
// Write a function that takes a string (a) and a number (n) as argument. Return the nth character of 'a'.
function myFunction(a, n) {
  return a[n - 1];
}

// Remove first n characters of string
// Write a function that takes a string (a) as argument. Remove the first 3 characters of a. Return the result
function myFunction(a) {
  return a.slice(3);
}

// Get last n characters of string
// Write a function that takes a string as argument. Extract the last 3 characters from the string. Return the result
function myFunction(a) {
  return a.slice(-3);
}

// Get first n characters of string
// Write a function that takes a string (a) as argument. Get the first 3 characters of a. Return the result
function myFunction(a) {
  return a.slice(0, 3);
}

// Find the position of one string in another
// Write a function that takes a string as argument. The string contains the substring 'is'. Return the index of 'is'.
function myFunction(a) {
  return a.indexOf('is');
}

// Extract first half of string
// Write a function that takes a string (a) as argument. Extract the first half a. Return the result
function myFunction(a) {
  return a.slice(0, a.length / 2);
}

// Remove last n characters of string
// Write a function that takes a string (a) as argument. Remove the last 3 characters of a. Return the result
function myFunction(a) {
  return a.slice(0, -3);
}

// Return the percentage of a number
// Write a function that takes two numbers (a and b) as argument. Return b percent of a
function myFunction(a, b) {
  return (b / 100) * a;
}

// Basic JavaScript math operators
// Write a function that takes 6 values (a,b,c,d,e,f) as arguments. Sum a and b. Then substract by c. Then multiply by d and divide by e. Finally raise to the power of f and return the result. Tipp: mind the order.
function myFunction(a, b, c, d, e, f) {
  return (((a + b - c) * d) / e) ** f;
}

// Check whether a string contains another string and concatenate
// Write a function that takes two strings (a and b) as arguments. If a contains b, append b to the beginning of a. If not, append it to the end. Return the concatenation
function myFunction(a, b) {
  return a.indexOf(b) === -1 ? a + b : b + a;
}

// Check if a number is even
// Write a function that takes a number as argument. If the number is even, return true. Otherwise, return false
function myFunction(a) {
  return a % 2 === 0;
}

// How many times does a character occur?
// Write a function that takes two strings (a and b) as arguments. Return the number of times a occurs in b.
function myFunction(a, b) {
  return b.split(a).length - 1;
}

// Check if a number is a whole number
// Write a function that takes a number (a) as argument. If a is a whole number (has no decimal place), return true. Otherwise, return false.
function myFunction(a) {
  return Number.isInteger(a);
}

// Multiplication, division, and comparison operators
// Write a function that takes two numbers (a and b) as arguments. If a is smaller than b, divide a by b. Otherwise, multiply both numbers. Return the resulting value
function myFunction(a, b) {
  return a < b ? a / b : a * b
}

// Round a number to 2 decimal places
// Write a function that takes a number (a) as argument. Round a to the 2nd digit after the decimal point. Return the rounded number
function myFunction(a) {
  return Number(a.toFixed(2));
}

// Split a number into its digits
// Write a function that takes a number (a) as argument. Split a into its individual digits and return them in an array. Tipp: you might want to change the type of the number for the splitting
function myFunction(a) {
  return Array.from(String(a), Number);
}

// JAVASCRIPT ARRAYS

// Get nth element of array
// Write a function that takes an array (a) and a value (n) as argument. Return the nth element of 'a'
function myFunction(a, n) {
  return a[n - 1];
}

// Remove first n elements of an array
// Write a function that takes an array (a) as argument. Remove the first 3 elements of 'a'. Return the result
function myFunction(a) {
  return a.slice(3);
}

// Get last n elements of an array
// Write a function that takes an array (a) as argument. Extract the last 3 elements of 'a'. Return the resulting array
function myFunction(a) {
  return a.slice(-3);
}

// Get first n elements of an array
// Write a function that takes an array (a) as argument. Extract the first 3 elements of a. Return the resulting array
function myFunction(a) {
  return a.slice(0, 3);
}

// Return last n array elements
// Write a function that takes an array (a) and a number (n) as arguments. It should return the last n elements of a.
function myFunction(a, n) {
  return a.slice(-n);
}

// Remove a specific array element
// Write a function that takes an array (a) and a value (b) as argument. The function should remove all elements equal to 'b' from the array. Return the filtered array.
function myFunction(a, b) {
  return a.filter((x) => x !== b);
}

// Count number of elements in JavaScript array
// Write a function that takes an array (a) as argument. Return the number of elements in a.
function myFunction(a) {
  return a.length;
}

// Count number of negative values in array
// Write a function that takes an array of numbers as argument. Return the number of negative values in the array.
function myFunction(a) {
  return a.filter((x) => x < 0).length;
}

//  Sort an array of strings alphabetically
// Write a function that takes an array of strings as argument. Sort the array elements alphabetically. Return the result.
function myFunction(arr) {
  return arr.sort()
}

// Sort an array of numbers in descending order
// Write a function that takes an array of numbers as argument. It should return an array with the numbers sorted in descending order.
function myFunction(arr) {
  return arr.sort((a, b) => b - a)
}

// Calculate the sum of an array of numbers
// Write a function that takes an array of numbers as argument. It should return the sum of the numbers.
function myFunction(a) {
  return a.reduce((x, y) => x + y)
}

// Return the average of an array of numbers
// Write a function that takes an array of numbers as argument. It should return the average of the numbers.
function myFunction(a) {
  return a.reduce((x, y) => x + y) / a.length
}

// Return the longest string from an array of strings
// Write a function that takes an array of strings as argument. Return the longest string.
function myFunction(a) {
  return arr.reduce((a, b) => a.length <= b.length ? b : a)
}

// Check if all array elements are equal
// Write a function that takes an array as argument. It should return true if all elements in the array are equal. It should return false otherwise.
function myFunction(a) {
  return a.every((x) => x === a[0])
}

// Merge an arbitrary number of arrays
// Write a function that takes arguments an arbitrary number of arrays. It should return an array containing the values of all arrays.
function myFunction(...arr) {
  return arr.flat()
}

// Sort array by object property
// Write a function that takes an array of objects as argument. Sort the array by property b in ascending order. Return the sorted array
function myFunction(arr) {
  return arr.sort((a, b) => a.b - b.b)
}

// Merge two arrays with duplicate values
// Write a function that takes two arrays as arguments. Merge both arrays and remove duplicate values. Sort the merge result in ascending order. Return the resulting array
function myFunction(a, b) {
  return [...new Set([...a, ...b])].sort((a, b) => a - b)
}

// JAVASCRIPT OBJECT

// Accessing object properties one
// Write a function that takes an object with two properties as argument. It should return the value of the property with key country.
function myFunction(obj) {
  return obj.country
}

// Accessing object properties two
// Write a function that takes an object with two properties as argument. It should return the value of the property with key 'prop-2'. Tipp: you might want to use the square brackets property accessor
function myFunction(obj) {
  return obj['prop-2']
}

// Accessing object properties three
// Write a function that takes an object with two properties and a string as arguments. It should return the value of the property with key equal to the value of the string
function myFunction(obj, key) {
  return obj[key]
}

// Check if property exists in object
// Write a function that takes an object (a) and a string (b) as argument. Return true if the object has a property with key 'b'. Return false otherwise. Tipp: test case 3 is a bit tricky because the value of property 'z' is undefined. But the property itself exists.
function myFunction(a, b) {
  return b in a
}

// Check if property exists in object and is truthy
// Write a function that takes an object (a) and a string (b) as argument. Return true if the object has a property with key 'b', but only if it has a truthy value. In other words, it should not be null or undefined or false. Return false otherwise.
function myFunction(a, b) {
  return Boolean(a[b]);
}

// Creating Javascript objects one
// Write a function that takes a string as argument. Create an object that has a property with key 'key' and a value equal to the string. Return the object.
function myFunction(a) {
  return { key: a }
}

// Creating Javascript objects two
// Write a function that takes two strings (a and b) as arguments. Create an object that has a property with key 'a' and a value of 'b'. Return the object.
function myFunction(a, b) {
  return { [a]: b }
}

// Creating Javascript objects three
// Write a function that takes two arrays (a and b) as arguments. Create an object that has properties with keys 'a' and corresponding values 'b'. Return the object.
// console.log(myFunction(['a', 'b', 'c'], [1, 2, 3])) /* Expected {a:1,b:2,c:3} */
// console.log(myFunction(['w', 'x', 'y', 'z'], [10, 9, 5, 2])) /* Expected {w:10,x:9,y:5,z:2} */
// console.log(myFunction([1, 'b'], ['a', 2])) /* Expected {1:'a',b:2} */
function myFunction(a, b) {
  return a.reduce((x, y, i) => ({ ...x, [y]: b[i] }), {});
}
// Explanation of the reduce method:
// The reduce() method executes a reducer function (that you provide) on each element of the array, resulting in a single output value.
// The reducer function takes four arguments:
// Accumulator (x)
// Current Value (y)
// Current Index (i)
// Source Array (a)
// ...x: spread operator to copy the accumulator object (x) into a new object (x) and add the new property (y) to it. The new object (x) is returned and assigned to the accumulator (x) for the next iteration. The first iteration starts with an empty object ({}).
// [y]: computed property name. The value of y is used as the property name.
// b[i]: the value of the property (y) is taken from the array (b) at the same index (i) as the property name (y).

// Extract keys from Javascript object
// Write a function that takes an object (a) as argument. Return an array with all object keys.
function myFunction(a) {
  return Object.keys(a)
}

// Return nested object property
// Write a function that takes an object as argument. In some cases the object contains other objects with some deeply nested properties. Return the property 'b' of object 'a' inside the original object if it exists. If not, return undefined.
// console.log(myFunction({ a: 1 })) /* Expected undefined */
// console.log(myFunction({ a: { b: { c: 3 } } })) /* Expected {c:3} */
// console.log(myFunction({ b: { a: 1 } })) /* Expected undefined */
// console.log(myFunction({ a: { b: 2 } })) /* Expected 2 */
function myFunction(obj) {
  return obj.a?.b
}

// Sum object values
// Write a function that takes an object (a) as argument. Return the sum of all object values.
// console.log(myFunction({ a: 1, b: 2, c: 3 })) /* Expected 6 */
// console.log(myFunction({ j: 9, i: 2, x: 3, z: 4 })) /* Expected 18 */
// console.log(myFunction({ w: 15, x: 22, y: 13 })) /* Expected 50 */
function myFunction(obj) {
  return Object.values(obj).reduce((x, y) => x + y)
}

// Remove a property from an object
// Write a function that takes an object as argument. It should return an object with all original object properties. except for the property with key 'b'.
function myFunction(obj) {
  delete obj.b;
  return obj;
}
// Alternative solution
// function myFunction(obj) {
//   const { b, ...rest } = obj;
//   return rest;
// }

// Merge two objects with matching keys
// Write a function that takes two objects as arguments. Unfortunately, the property 'b' in the second object has the wrong key. It should be named 'd' instead. Merge both objects and correct the wrong property name. Return the resulting object. It should have the properties 'a', 'b', 'c', 'd', and 'e'
// console.log(myFunction({ a: 1, b: 2 }, { c: 3, b: 4, e: 5 })) /* { a: 1, b: 2, c: 3, e: 5, d: 4} */
// console.log(myFunction({ a: 5, b: 4 }, { c: 3, b: 1, e: 2 })) /* { a: 5, b: 4, c: 3, e: 2, d: 1} */
function myFunction(x, y) {
  const { b, ...rest } = y;
  return { ...x, ...rest, d: b };
}

// Multiply all object values by x
// Write a function that takes an object (a) and a number (b) as arguments. Multiply all values of 'a' by 'b'. Return the resulting object.
// console.log(myFunction({ a: 1, b: 2, c: 3 }, 3)) /* {a:3,b:6,c:9} */
// console.log({ j: 9, i: 2, x: 3, z: 4 }, 10) /* {j:90,i:20,x:30,z:40} */
// console.log(myFunction({ w: 15, x: 22, y: 13 }, 6)) /* {w:90,x:132,y:78} */
function myFunction(a, b) {
  return Object.fromEntries(Object.entries(a).map(([x, y]) => [x, y * b]));
}
// Alternative solution
// function myFunction(a, b) {
//   return Object.entries(a).reduce((acc, [key, val]) => {
//     return { ...acc, [key]: val * b };
//   }, {});
// }

// JAVASCRIPT DATES

// Check if two dates are equal
// Sounds easy, but you need to know the trick. Write a function that takes two date instances as arguments. It should return true if the dates are equal. It should return false otherwise.
function myFunction(a, b) {
  return a.getTime() === b.getTime()
}

// Return the number of days between two dates
// Write a function that takes two date instances as argument. It should return the number of days that lies between those dates.
function myFunction(a, b) {
  return Math.abs(a.getTime() - b.getTime()) / 86400000
}
// Explanation of the formula:
// 1 day = 24 hours
// 1 hour = 60 minutes
// 1 minute = 60 seconds
// 1 second = 1000 milliseconds
// 24 * 60 * 60 * 1000 = 86400000

// Alternative solution
function myFunction(a, b) {
  // return Math.abs(a - b) / 86400000;
  const dif = Math.abs(a - b);
  return dif / 1000 / 60 / 60 / 24
}

// Check if two dates fall on the exact same day
// Write a function that takes two date instances as argument. It should return true if they fall on the exact same day. It should return false otherwise.
function myFunction(a, b) {
  return a.toDateString() === b.toDateString()
}
// Alternative solution
// function myFunction(a, b) {
//   return a.getFullYear() === b.getFullYear() &&
//     a.getMonth() === b.getMonth() &&
//     a.getDate() === b.getDate()
// }

// Check if two dates are within 1 hour from each other
// Write a function that takes two date instances as argument. It should return true if the difference between the dates is less than or equal to 1 hour. It should return false otherwise.
function myFunction(a, b) {
  return Math.abs(a - b) <= 3600000
}
// Explanation of the formula:
// 1 hour = 60 minutes
// 1 minute = 60 seconds
// 1 second = 1000 milliseconds
// 60 * 60 * 1000 = 3600000

// Check if one date is earlier than another
// Write a function that takes two date instances (a and b) as arguments. It should return true if a is earlier than b. It should return false otherwise.
// console.log(myFunction(new Date('2000/01/01 08:00:00'), new Date('2000/01/01 08:45:00'))) /* true */
// console.log(myFunction(new Date('2000/01/01 08:45:00'), new Date('2000/01/01 08:00:00'))) /* false */
// console.log(myFunction(new Date('2000/01/01 08:00:00'), new Date('2000/01/01 08:00:00'))) /* false */

function myFunction(a, b) {
  // return a.getTime() < b.getTime()
  return a < b
}

// JAVASCRIPTS SETS

// Check if value is present in Set
// Write a function that takes a Set and a value as arguments. Check if the value is present in the Set.
function myFunction(set, val) {
  return set.has(val)
}

// Convert a Set to Array
// Write a function that takes a Set as argument. Convert the Set to an Array. Return the Array.
function myFunction(set) {
  return [...set]
}

// Get union of two sets
// Write a function that takes two Sets as arguments. Create the union of the two sets. Return the result. Tipp: try not to switch to Arrays, this would slow down your code.
function myFunction(a, b) {
  return new Set([...a, ...b])
  // const result = new Set(a);
  // b.forEach((el) => result.add(el));
  // return result;
}

// Creating Javascript Sets
// Write a function that takes three elements of any type as arguments. Create a Set from those elements. Return the result.
function myFunction(a, b, c) {
  return new Set([a, b, c])
  // const set = new Set();
  // set.add(a);
  // set.add(b);
  // set.add(c);
  // return set;
}

// Delete element from Set
// Write a function that takes a Set and a value as argument. If existing in the Set, remove the value from the Set. Return the result.
function myFunction(set, val) {
  set.delete(val);
  return set;
}
const response = require('./hero.json')

// 1. display list hero name from hero.json
// 2. display list hero name from hero.json then sort ascending
const hero = response.hero.map((item) => {
  return item.hero_name;
});
console.log('1. display list hero name from hero.json:', hero)
console.log('2. display list hero name from hero.json then sort ascending:', hero.sort())

// 3. display all hero with role tank
const roleTank = response.hero.filter((item) => {
  return item.hero_role.includes('Tank');
}).map((item) => {
  return `${item.hero_name} - ${item.hero_role}`
})
console.log('3. display all hero with role tank:', roleTank)

// 4. display all hero with specially crowd control
const heroCrowdControl = response.hero.filter((item) => {
  return item.hero_specially.includes('Crowd Control')
}).map((item) => {
  return `${item.hero_name} - ${item.hero_specially}`
})
console.log('4. display all hero with specially crowd control:', heroCrowdControl)

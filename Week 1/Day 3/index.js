import fetch from "node-fetch";

// Create countdown program to display remaining time, input will be number in second
// Example: countdown(5402);
// Expected Output:
// 01:30:02
// 01:30:01
// 01:30:00
// 01:29:59
// ..
// 00:00:00

// const countdown = (seconds) => {
//   let timer = seconds

//   setInterval(() => {
//     let result = new Date(timer * 1000).toISOString().slice(11, 19);
//     console.log(result);
//     timer--;
//   }, 1000)

//   let hours = Math.floor(seconds / 3600);
//   let minutes = Math.floor((seconds % 3600) / 60);
//   let second = Math.floor((seconds % 3600) % 60);

//   let time = `${hours}:${minutes}:${second}`;

//   const x = setInterval(() => {
//     if (second > 0) {
//       second--;
//     } else {
//       second = 59;
//       if (minutes > 0) {
//         minutes--;
//       } else {
//         minutes = 59;
//         if (hours > 0) {
//           hours--;
//         } else {
//           hours = 0;
//         }
//       }
//     }
//     if (hours <= 0 && minutes <= 0 && second <= 0) {
//       clearInterval(x)
//     }
//     time = `${hours}:${minutes}:${second}`;
//     console.log(time);
//   }, 1000);
// }
// countdown(100);

// Create program to display list recommendation anime. (you can fetching data from url: https://api.jikan.moe/v4/recommendations/anime )
// Show all list title
// Sort title by date release
// Show 5 most popular anime from recommendation
// Show 5 high rank anime from recommendation
// Show most episodes anime from recommendation

const response = await fetch('https://api.jikan.moe/v4/recommendations/anime ')
const responseJson = await response.json()
const data = responseJson?.data

// Limit list from list data from response to 20
const limit = data?.slice(0, 10)


// 1. Show all list title
const allListTitle = () => {
  const title = limit.map((item) => item.entry.map((item) => item.title))
  // merge array1
  const merged = title.flat();
  console.log(merged);
}




// 2.1 Sort title by date release
const titleByDateRelease = () => {
  const date = limit.map((item) => item.date)
  const sorted = date.sort((a, b) => a - b)

  const titleByDate = sorted.map((item) => {
    const resultFilter = limit.filter((item2) => item2.date === item)
    const result = resultFilter.map((item3) => item3.entry.map((item4) => item4.title))
    const merged = result.flat();
    return merged
  })

  // const result = titleByDate.flat();

  const sortedTitles = limit
    .sort((a, b) => a.date - b.date)
    .reduce((titlesByDate, item) => {
      const date = item.date;
      const titles = item.entry.map((entry) => entry.title);
      titlesByDate[date] = titlesByDate[date] ? [...titlesByDate[date], ...titles] : titles;
      return titlesByDate;
    }, {});

  const result = Object.values(sortedTitles).flat();

  console.log(result);
}



// 2.2 Show 5 most popular anime title from recommendation by "mal_id" sort by "dataPopular.data.popularity"
const popularAnimeTitle = () => {
  const fetchAnimeData = async (mal_id) => {
    try {
      const resById = await fetch(`https://api.jikan.moe/v4/anime/${mal_id}`);
      const dataPopular = await resById.json();
      const title = dataPopular?.data?.title;
      const popularity = dataPopular?.data?.popularity;
      return [title, popularity];
    } catch (error) {
      console.log('Error occurred during fetching:', error);
      return null;
    }
  };

  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  const fetchPromises = limit.flatMap(async (item, index) => {
    const itemPromises = item.entry.map(async (entry) => {
      await delay(index % 20 === 0 ? 60000 : 1000); // Delay every 20 requests for 60 seconds, otherwise delay for 1 second
      return fetchAnimeData(entry.mal_id);
    });
    return Promise.all(itemPromises);
  });

  Promise.all(fetchPromises)
    .then((results) => {
      const animeArray = results.flat().filter((item) => item !== null);
      console.log('Fetched data:');
      console.table(animeArray);
      console.log('All data fetched successfully.');
    })
    .catch((error) => {
      console.log('Error occurred during fetching:', error);
    });
};


// allListTitle()
// titleByDateRelease()
popularAnimeTitle()
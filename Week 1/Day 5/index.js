// 1. Create a function that changes specific words into emoticons. Given a sentence as a string, replace the words smile, grin, sad and mad with their corresponding emoticons

// word	emoticon
// smile	:D
// grin	:)
// sad	:(
// mad	:P

// Notes
// The sentence always starts with "Make me".
// Try to solve this without using conditional statements like if/else or switch.

// Examples
// emotify("Make me smile") ➞ "Make me :D"
// emotify("Make me grin") ➞ "Make me :)"
// emotify("Make me sad") ➞ "Make me :("
// emotify("Make me smile") ➞ "Make me :D"
// emotify("Make me grin") ➞ "Make me :)"
// emotify("Make me sad") ➞ "Make me :("
// emotify("Make me mad") ➞ "Make me :P" 

const emotify = (str) => {
  const str2 = str.split(" ");
  for (let i = 0; i < str2.length; i++) {
    if (str2[i] === "smile") {
      str2[i] = ":D";
    } else if (str2[i] === "grin") {
      str2[i] = ":)";
    } else if (str2[i] === "sad") {
      str2[i] = ":(";
    } else if (str2[i] === "mad") {
      str2[i] = ":P";
    }
  }
  const str3 = str2.join(" ");
  console.log(str3)
}

// emotify("Make me smile") /* Make me :D */
// emotify("Make me grin") /* ➞ "Make me :)" */
// emotify("Make me sad") /* ➞ "Make me :(" */
// emotify("Make me smile") /* ➞ "Make me :D" */
// emotify("Make me grin") /* ➞ "Make me :)" */
// emotify("Make me sad") /* ➞ "Make me :(" */
// emotify("Make me mad") /* ➞ "Make me :P" */



// 2. Create a function that takes a string as an argument and converts the first character of each word to uppercase. Return the newly formatted string.

// Notes
// You can expect a valid string for each test case.
// Some words may contain more than one uppercase letter (see example #4)

const makeTitle = (str) => {
  const str2 = str.split(" ");

  for (let i = 0; i < str2.length; i++) {
    str2[i] = str2[i][0].toUpperCase() + str2[i].substr(1);
  }

  const str3 = str2.join(" ");
  console.log(str3)
}
// makeTitle("This is a title") /* ➞ "This Is A Title" */
// makeTitle("capitalize every word") /* ➞ "Capitalize Every Word" */
// makeTitle("I Like Pizza") /* ➞ "I Like Pizza" */
// makeTitle("PIZZA PIZZA PIZZA") /* ➞ "PIZZA PIZZA PIZZA" */
// makeTitle("the first letter of every word is capitalized") /* ➞ "The First Letter Of Every Word Is Capitalized" */
// makeTitle("I Like Pizza") /* ➞ "I Like Pizza" */
// makeTitle("Don't count your ChiCKens BeFore They HatCh") /* ➞ "Don't Count Your ChiCKens BeFore They HatCh" */



// 3. Create a function that takes an array of numbers between 1 and 10 (excluding one number) and returns the missing number.
// Notes
// The array of numbers will be unsorted (not in order).
// Only one number will be missing.

const missingNum = (num, n = 10) => {
  const count = num.reduce((a, b) => a + b, 0);
  const sum = (n * (n + 1)) / 2;
  console.log(sum - count)
}

// Examples
// missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]) /* ➞ 5 */
// missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]) /* ➞ 10 */
// missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]) /* ➞ 7 */
// missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]) /* ➞ 5 */
// missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]) /* ➞ 10 */



// 4. Create a function that takes an array of items, removes all duplicate items and returns a new array in the same sequential order as the old array (minus duplicates).
// Notes
// Tests contain arrays with both strings and numbers.
// Tests are case sensitive.
// Each array item is unique.
// Examples
// removeDups([1, 0, 1, 0]) ➞ [1, 0]
// removeDups(["The", "big", "cat"]) ➞ ["The", "big", "cat"]
// removeDups(["John", "Taylor", "John"]) ➞ ["John", "Taylor"]
// removeDups(['John', 'Taylor', 'John', 'john']) ➞ ['John', 'Taylor', 'john']
// removeDups(['javascript', 'python', 'python', 'ruby', 'javascript', 'c', 'ruby']) ➞ ['javascript', 'python', 'ruby', 'c']

const removeDups = (arr) => {
  const newArr = [];
  for (let i = 0; i < arr.length; i++) {
    if (!newArr.includes(arr[i])) {
      newArr.push(arr[i])
    }
  }
  console.log(newArr)
}

// removeDups([1, 0, 1, 0]) /* ➞ [1, 0] */
// removeDups(["The", "big", "cat"]) /* ➞ ["The", "big", "cat"] */
// removeDups(["John", "Taylor", "John"]) /* ➞ ["John", "Taylor"] */
// removeDups(['John', 'Taylor', 'John', 'john']) /* ➞ ['John', 'Taylor', 'john'] */
// removeDups(['javascript', 'python', 'python', 'ruby', 'javascript', 'c', 'ruby']) /* ➞ ['javascript', 'python', 'ruby', 'c'] */



// 5. Create the function that takes an array with objects and returns the sum of people's budgets.
// Examples
// getBudgets([
//   { name: "John", age: 21, budget: 23000 },
//   { name: "Steve",  age: 32, budget: 40000 },
//   { name: "Martin",  age: 16, budget: 2700 }
// ]) ➞ 65700

// getBudgets([
//   { name: "John",  age: 21, budget: 29000 },
//   { name: "Steve",  age: 32, budget: 32000 },
//   { name: "Martin",  age: 16, budget: 1600 }
// ]) ➞ 62600

const getBudgets = (arr) => {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i].budget;
  }
  console.log(sum)
}

// getBudgets([{ name: "John", age: 21, budget: 23000 }, { name: "Steve", age: 32, budget: 40000 }, { name: "Martin", age: 16, budget: 2700 }]) /* ➞ 65700) */
// getBudgets([{ name: "John", age: 21, budget: 29000 }, { name: "Steve", age: 32, budget: 32000 }, { name: "Martin", age: 16, budget: 1600 }]) /* ➞ 62600) */
// getBudgets([{ name: "John", age: 21, budget: 19401 }, { name: "Steve", age: 32, budget: 12321 }, { name: "Martin", age: 16, budget: 1204 }]) /* ➞ 32926) */
// getBudgets([{ name: "John", age: 21, budget: 10234 }, { name: "Steve", age: 32, budget: 21754 }, { name: "Martin", age: 16, budget: 4935 }]) /* ➞ 36923) */



// 6. An array is special if every even index contains an even number and every odd index contains an odd number. Create a function that returns true if an array is special, and false otherwise.
// Examples
// isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]) ➞ true // Even indices: [2, 4, 6, 6]; Odd indices: [7, 9, 1, 3]
// isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]) ➞ false // Index 2 has an odd number 9.
// isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]) ➞ false // Index 3 has an even number 8.
// isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]) ➞ true
// isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]) ➞ false
// isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]) ➞ false

const isSpecialArray = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (i % 2 === 0 && arr[i] % 2 !== 0) {
      return false;
    } else if (i % 2 !== 0 && arr[i] % 2 === 0) {
      return false;
    }
  }
  return true;
}

// console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3])) /* ➞ true */ // Even indices: [2, 4, 6, 6]; Odd indices: [7, 9, 1, 3]
// console.log(isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3])) /* ➞ false */ // Index 2 has an odd number 9.
// console.log(isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3])) /* ➞ false */ // Index 3 has an even number 8.
// console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3])) /* ➞ true */
// console.log(isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3])) /* ➞ false */
// console.log(isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3])) /* ➞ false */



// 7. Given an input string, reverse the string word by word, the first word will be the last, and so on.
// Notes
// A word is defined as a sequence of non-space characters.
// The input string may contain leading or trailing spaces. However, your reversed string should not contain leading or trailing spaces.
// You need to reduce multiple spaces between two words to a single space in the reversed string.
// Try to solve this in linear time.

// Examples
// reverseWords(" the sky is blue") ➞ "blue is sky the"
// reverseWords("hello   world!  ") ➞ "world! hello"
// reverseWords("a good example") ➞ "example good a"
// reverseWords("hello world!") ➞ "world! hello")
// reverseWords("blue is sky the") ➞ "the sky is blue")

const reverseWords = (str) => {
  const str2 = str.split(" ");
  const str3 = str2.reverse();
  const str4 = str3.join(" ").replace(/\s+/g, ' ').trim();
  console.log(str4)
}

// reverseWords(" the sky is blue") /* ➞ "blue is sky the" */
// reverseWords("hello   world!  ") /* ➞ "world! hello" */
// reverseWords("a good example") /* ➞ "example good a" */
// reverseWords("hello world!") /* ➞ "world! hello") */
// reverseWords("blue is sky the") /* ➞ "the sky is blue") */
// reverseWords("a good example") /* ➞ "example good a") */

// 8. Create a function that returns true if an asterisk * is inside a box.
// Notes
// The asterisk may be in the array, however, it must be inside the box, if it exists.

// Examples
// inBox([
//   "###",
//   "#*#",
//   "###"
// ]) ➞ true

// inBox([
//   "####",
//   "#* #",
//   "#  #",
//   "####"
// ]) ➞ true

// inBox([
//   "*####",
//   "# #",
//   "#  #*",
//   "####"
// ]) ➞ false

// inBox([
//   "#####",
//   "#   #",
//   "#   #",
//   "#   #",
//   "#####"
// ]) ➞ false

// inBox([
// "###", 
// "# #", 
// "###"
// ]) ➞ false

const inBox = (arr) => {
  for (i = 0; i < arr.length; i++) {
    if (arr[i].includes('*') && arr[i][0] === "#" && arr[i][arr[i].length - 1] === '#') {
      return true
    }
  }
  return false
}

// console.log(inBox(["###", "#*#", "###"])) /* ➞ true */
// console.log(inBox(["####", "#* #", "#  #", "####"])) /* ➞ true */
// console.log(inBox(["*####", "# #", "#  #*", "####"])) /* ➞ false */
// console.log(inBox(["#####", "#   #", "#   #", "#   #", "#####"])) /* ➞ false */
// console.log(inBox(["###", "# #", "###"])) /* ➞ false */



// 9. You are given a dictionary of names and the amount of points they have. Return a dictionary with the same names, but instead of points, return what prize they get.
// "Gold", "Silver", or "Bronze" to the 1st, 2nd and 3rd place respectively. For all the other names, return "Participation" for the prize.

// Notes
// There will always be at least three participants to recieve awards.
// No number of points will be the same amongst participants.

// Examples
// awardPrizes({
//   "Joshua" : 45,
//   "Alex" : 39,
//   "Eric" : 43
// }) ➞ {
//   "Joshua" : "Gold",
//   "Alex" : "Bronze",
//   "Eric" : "Silver"
// }

// awardPrizes({
//   "Person A" : 1,
//   "Person B" : 2,
//   "Person C" : 3,
//   "Person D" : 4,
//   "Person E" : 102
// }) ➞ {
//   "Person A" : "Participation",
//   "Person B" : "Participation",
//   "Person C" : "Bronze",
//   "Person D" : "Silver",
//   "Person E" : "Gold"
// }

// awardPrizes({
//   "Mario" : 99,
//   "Luigi" : 100,
//   "Yoshi" : 299,
//   "Toad" : 2
// }) ➞ {
//   "Mario" : "Bronze",
//   "Luigi" : "Silver",
//   "Yoshi" : "Gold",
//   "Toad" : "Participation"
// }

// awardPrizes({
// 	'Joshua' : 45,
// 	'Alex' : 39,
// 	'Eric' : 43
// }) ➞ {
// 	'Joshua' : 'Gold',
// 	'Alex' : 'Bronze',
// 	'Eric' : 'Silver'
// }

const awardPrizes = (obj) => {
  const arr = Object.entries(obj);
  const arr2 = arr.sort((a, b) => b[1] - a[1]);
  const arr3 = arr2.map((item, index) => {
    if (index === 0) {
      return [item[0], "Gold"]
    } else if (index === 1) {
      return [item[0], "Silver"]
    } else if (index === 2) {
      return [item[0], "Bronze"]
    } else {
      return [item[0], "Participation"]
    }
  })
  const obj2 = Object.fromEntries(arr3);
  return obj2;
}

// console.log(awardPrizes({ "Joshua": 45, "Alex": 39, "Eric": 43 }))
// console.log(awardPrizes({ "Person A": 1, "Person B": 2, "Person C": 3, "Person D": 4, "Person E": 102 }))
// console.log(awardPrizes({ "Mario": 99, "Luigi": 100, "Yoshi": 299, "Toad": 2 }))
// console.log(awardPrizes({ 'Joshua': 45, 'Alex': 39, 'Eric': 43 }))



// 10. Create a function that determines how many number pairs are embedded in a space-separated string. The first numeric value in the space-separated string represents the count of the numbers, thus, excluded in the pairings.

// Notes
// Always take into consideration the first number in the string is not part of the pairing, thus, the count. It may not seem so useful as most people see it, but it's mathematically significant if you deal with set operations.

// Examples
// number_pairs("7 1 2 1 2 1 3 2") ➞ 2 // (1, 1) ➞ (2, 2)
// number_pairs("9 10 20 20 10 10 30 50 10 20") ➞ 3 // (10, 10) ➞ (20, 20) ➞ (10, 10)
// number_pairs("4 2 3 4 1") ➞ 0 // Although two 4's are present, the first one is discounted.

const number_pairs = (str) => {
  const str2 = str.split(" ");
  const str3 = str2.slice(1);
  const obj = {};
  let count = 0;
  for (let i = 0; i < str3.length; i++) {
    if (obj[str3[i]]) {
      count++;
      obj[str3[i]] = 0;
    } else {
      obj[str3[i]] = 1;
    }
  }
  return count;
}

// console.log(number_pairs("7 1 2 1 2 1 3 2"))
// console.log(number_pairs("9 10 20 20 10 10 30 50 10 20"))
// console.log(number_pairs("4 2 3 4 1"))



// 11. Abigail and Benson are playing Rock, Paper, Scissors.
// Each game is represented by an array of length 2, where the first element represents what Abigail played and the second element represents what Benson played.

// Given a sequence of games, determine who wins the most number of matches. If they tie, output "Tie".

// R stands for Rock
// P stands for Paper
// S stands for Scissors

// Examples
// calculateScore([["R", "P"], ["R", "S"], ["S", "P"]]) ➞ "Abigail"

// Benson wins the first game (Paper beats Rock).
// Abigail wins the second game (Rock beats Scissors).
// Abigail wins the third game (Scissors beats Paper).
// Abigail wins 2/3.

// calculateScore([["R", "R"], ["S", "S"]]) ➞ "Tie"
// calculateScore([["S", "R"], ["R", "S"], ["R", "R"]]) ➞ "Tie"
// calculateScore([['R', 'P'], ['R', 'S'], ['S', 'P']]) ➞  "Abigail"
// calculateScore([['R', 'R'], ['S', 'S']]) ➞ "Tie"
// calculateScore([['S', 'R'], ['R', 'S'], ['R', 'R']]) ➞ "Tie"
// calculateScore([['S', 'R'], ['P', 'R']]) ➞ "Tie"
// calculateScore([['S', 'S'], ['S', 'P'], ['R', 'S'], ['S', 'R'], ['R', 'R']]) ➞ "Abigail"
// calculateScore([['S', 'R'], ['S', 'R'], ['S', 'R'], ['R', 'S'], ['R', 'S']]) ➞ "Benson"

const calculateScore = (arr) => {
  let abigail = 0;
  let benson = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][0] === "R" && arr[i][1] === "P") {
      benson++;
    } else if (arr[i][0] === "R" && arr[i][1] === "S") {
      abigail++;
    } else if (arr[i][0] === "S" && arr[i][1] === "P") {
      abigail++;
    } else if (arr[i][0] === "S" && arr[i][1] === "R") {
      benson++;
    } else if (arr[i][0] === "P" && arr[i][1] === "R") {
      abigail++;
    } else if (arr[i][0] === "P" && arr[i][1] === "S") {
      benson++;
    }
  }
  return abigail > benson ? "Abigail" : abigail < benson ? "Benson" : "Tie";
}

// console.log(calculateScore([["R", "R"], ["S", "S"]])) /* ➞ "Tie" */
// console.log(calculateScore([["S", "R"], ["R", "S"], ["R", "R"]])) /* ➞ "Tie" */
// console.log(calculateScore([['R', 'P'], ['R', 'S'], ['S', 'P']])) /* ➞  "Abigail" */
// console.log(calculateScore([['R', 'R'], ['S', 'S']])) /* ➞ "Tie" */
// console.log(calculateScore([['S', 'R'], ['R', 'S'], ['R', 'R']])) /* ➞ "Tie" */
// console.log(calculateScore([['S', 'R'], ['P', 'R']])) /* ➞ "Tie" */
// console.log(calculateScore([['S', 'S'], ['S', 'P'], ['R', 'S'], ['S', 'R'], ['R', 'R']])) /* ➞ "Abigail" */
// console.log(calculateScore([['S', 'R'], ['S', 'R'], ['S', 'R'], ['R', 'S'], ['R', 'S']])) /* ➞ "Benson" */



// 12. Given a matrix of m * n elements (m rows, n columns) ➞ return all elements of the matrix in spiral order.

// Examples
// spiralOrder([
//   [ 1, 2, 3 ],
//   [ 4, 5, 6 ],
//   [ 7, 8, 9 ]
// ]) ➞ [1, 2, 3, 6, 9, 8, 7, 4, 5]

// spiralOrder([
//   [1, 2, 3, 4],
//   [5, 6, 7, 8],
//   [9,10,11,12]
// ]) ➞ [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]

// spiralOrder([
//  [ 1, 2, 3 ],
//  [ 4, 5, 6 ],
//  [ 7, 8, 9 ]
// ]) ➞ [1,2,3,6,9,8,7,4,5]

const spiralOrder = (arr) => {
  const newArr = [];
  while (arr.length) {
    newArr.push(...arr.shift());
    arr.map((item) => {
      newArr.push(item.pop());
    })
    newArr.push(...(arr.pop() || []).reverse());
    arr
  }
  return newArr;
}

// console.log(spiralOrder([[1, 2, 3], [4, 5, 6], [7, 8, 9]])) /* ➞ [1, 2, 3, 6, 9, 8, 7, 4, 5] */
// console.log(spiralOrder([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])) /* ➞ [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7] */
// console.log(spiralOrder([[1, 2, 3], [4, 5, 6], [7, 8, 9]])) /* ➞ [1,2,3,6,9,8,7,4,5] */



// 13. A countdown sequence is a descending sequence of k integers from k down to 1 (e.g. 5, 4, 3, 2, 1). Write a function that returns an array of the form[x, y] where x is the number of countdown sequences in the given array and y is the array of those sequences in the order in which they appear in the array.
// Notes
// A disjoint 1 is a valid countdown sequence. See examples #3 & #4.
// All numbers in the array will be greater than 0.

// Examples
// finalCountdown([4, 8, 3, 2, 1, 2]) ➞ [1, [[3, 2, 1]]]
// 1 countdown sequence: 3, 2, 1
// finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]) ➞ [2, [[5, 4, 3, 2, 1], [3, 2, 1]]]
// 2 countdown sequences:
// 5, 4, 3, 2, 1 and 3, 2, 1
// finalCountdown([4, 3, 2, 1, 3, 2, 1, 1]) ➞ [3, [[4, 3, 2, 1], [3, 2, 1], [1]]]
// 3 countdown sequences:
// 4, 3, 2, 1 ; 3, 2, 1 and 1

const finalCountdown = (arr) => {
  const newArr = [];
  let count = 0;
  let arr2 = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] - 1 === arr[i + 1]) {
      count++;
      arr2.push(arr[i]);
    } else if (arr[i] - 1 !== arr[i + 1] && count > 0) {
      count++;
      arr2.push(arr[i]);
      newArr.push(arr2);
      arr2 = [];
      count = 0;
    }
  }
  return [newArr.length, newArr];
};

console.log(finalCountdown([4, 8, 3, 2, 1, 2])) /* ➞ [1, [[3, 2, 1]]] */
console.log(finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1])) /* ➞ [2, [[5, 4, 3, 2, 1], [3, 2, 1]]] */
console.log(finalCountdown([4, 3, 2, 1, 3, 2, 1, 1])) /* ➞ [3, [[4, 3, 2, 1], [3, 2, 1], [1]]] */
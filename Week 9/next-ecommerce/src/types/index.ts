export interface NavLinkProps {
  className?: string
  activeClassName?: string
  href: string
  children: React.ReactNode
}

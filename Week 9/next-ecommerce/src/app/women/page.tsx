'use client'

import Image from 'next/image'
import { useState } from 'react'
import { addProduct } from '@/redux/features/Product/productSlice'
import { nanoid } from 'nanoid'
import { useAppDispatch } from '@/redux/hooks'

const productImages = [
  {
    src: '/images/image-product-1.jpg',
    thumbnail: '/images/image-product-1-thumbnail.jpg',
    alt: 'product',
    width: 450,
    height: 450,
  },
  {
    src: '/images/image-product-2.jpg',
    thumbnail: '/images/image-product-2-thumbnail.jpg',
    alt: 'product',
    width: 450,
    height: 450,
  },
  {
    src: '/images/image-product-3.jpg',
    thumbnail: '/images/image-product-3-thumbnail.jpg',
    alt: 'product',
    width: 450,
    height: 450,
  },
  {
    src: '/images/image-product-4.jpg',
    thumbnail: '/images/image-product-4-thumbnail.jpg',
    alt: 'product',
    width: 450,
    height: 450,
  },
]

const Women = () => {
  const [count, setCount] = useState(0)
  const [selectedImage, setSelectedImage] = useState(productImages[0])
  const [isOpen, setOpen] = useState<Boolean>(false)
  const dispatch = useAppDispatch()

  const handleImageClick = (image: any) => {
    setSelectedImage(image)
  }

  const handleOpen = () => {
    setOpen(!isOpen)
  }

  const handleNextImage = () => {
    const index = productImages.findIndex((image) => image.thumbnail === selectedImage.thumbnail)
    if (index === productImages.length - 1) {
      setSelectedImage(productImages[0])
    } else {
      setSelectedImage(productImages[index + 1])
    }
  }

  const handlePreviousImage = () => {
    const index = productImages.findIndex((image) => image.thumbnail === selectedImage.thumbnail)
    if (index === 0) {
      setSelectedImage(productImages[productImages.length - 1])
    } else {
      setSelectedImage(productImages[index - 1])
    }
  }

  const increment = () => {
    setCount((prevCount) => prevCount + 1)
  }

  const decrement = () => {
    if (count > 0) {
      setCount((prevCount) => prevCount - 1)
    }
  }

  const handleAddToCart = (counter: number): void => {
    if (counter === 0) return

    const product = {
      id: nanoid(),
      name: 'Fall Limited Edition Sneakers',
      thumbnail: '/images/image-product-1.jpg',
      price: 125,
      amount: counter,
    }
    dispatch(addProduct(product))
  }
  return (
    <div className='dark:bg-slate-700 container mx-auto min-h-screen sm:min-h-fit'>
      <div className='flex flex-col sm:flex-row items-center sm:mt-8 sm:mx-[4.5rem] sm:gap-32 gap-6 relative'>
        <div className='flex flex-col gap-5 relative'>
          <Image onClick={handleOpen} src={selectedImage.src} alt='product' width={450} height={450} className='sm:rounded-xl w-full sm:w-[450px]' />
          <div
            className='bg-white/50 absolute right-2 top-44 sm:hidden cursor-pointer rounded-full px-3 py-2 flex items-center justify-center'
            onClick={handleNextImage}>
            <Image src='/images/icon-next.svg' alt='next' width={10} height={10} />
          </div>
          <div
            className='bg-white/50 absolute left-2 top-44 sm:hidden cursor-pointer rounded-full px-3 py-2 flex items-center justify-center'
            onClick={handlePreviousImage}>
            <Image src='/images/icon-previous.svg' alt='previous' width={10} height={10} />
          </div>
          <div className='sm:flex item-center gap-5 w-full justify-around hidden'>
            {productImages.map(({ thumbnail, alt }, index) => (
              <div
                key={index}
                className={`${selectedImage.thumbnail === thumbnail ? 'border-2 border-primary' : ''} rounded-2xl cursor-pointer overflow-hidden`}>
                <Image
                  src={thumbnail}
                  alt={alt}
                  width={90}
                  height={90}
                  className={`rounded-xl ${selectedImage.thumbnail === thumbnail ? 'opacity-50' : ''}`}
                  onClick={() => handleImageClick(productImages[index])}
                />
              </div>
            ))}
          </div>
        </div>

        {isOpen && (
          <div className='fixed w-full bg-black/80 top-0 left-0 sm:flex items-center min-h-screen justify-center hidden'>
            <div className='flex flex-col gap-5 relative'>
              <div className='relative'>
                <Image
                  src='/images/icon-close.svg'
                  alt='close'
                  width={20}
                  height={20}
                  onClick={() => setOpen(!isOpen)}
                  className='absolute -top-2 right-0 cursor-pointer'
                />
                <div
                  className='bg-white absolute -right-6 top-56 cursor-pointer rounded-full px-3 py-2 flex items-center justify-center'
                  onClick={handleNextImage}>
                  <Image src='/images/icon-next.svg' alt='next' width={20} height={20} />
                </div>
                <div
                  className='bg-white absolute -left-6 top-56 cursor-pointer rounded-full px-3 py-2 flex items-center justify-center'
                  onClick={handlePreviousImage}>
                  <Image src='/images/icon-previous.svg' alt='previous' width={20} height={20} />
                </div>
              </div>
              <Image src={selectedImage.src} alt='product' width={450} height={450} className='sm:rounded-xl w-full sm:w-[450px]' />
              <div className='sm:flex item-center gap-5 w-full justify-around hidden'>
                {productImages.map(({ thumbnail, alt }, index) => (
                  <div
                    key={index}
                    className={`${
                      selectedImage.thumbnail === thumbnail ? 'border-2 border-primary' : ''
                    } rounded-2xl cursor-pointer overflow-hidden`}>
                    <Image
                      src={thumbnail}
                      alt={alt}
                      width={90}
                      height={90}
                      className={`rounded-xl ${selectedImage.thumbnail === thumbnail ? 'contrast-50' : ''}`}
                      onClick={() => handleImageClick(productImages[index])}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}

        <div className='flex flex-col sm:w-1/2 mx-6'>
          <div className='text-primary uppercase font-semibold'>Sneaker Company</div>
          <h1 className='text-3xl font-bold'>Fall Limited Edition Sneakers</h1>
          <p className='text-gray-500 mt-5'>
            These low-profile sneakers are your perfect casual wear companion. Featuring a durable rubber outer sole, they’ll withstand everything the
            weather can offer.
          </p>
          <div className='flex flex-row sm:flex-col items-center sm:items-start justify-between sm:justify-normal gap-2 mt-5 text-3xl font-bold'>
            <div className='flex items-center gap-5'>
              $125.00 <span className='bg-orange-100 text-lg text-orange-600 px-3 py-1 font-semibold rounded'>50%</span>
            </div>
            <div className='line-through text-lg'>$250.00</div>
          </div>
          <div className='flex flex-col sm:flex-row items-center gap-5 mt-5 w-full mb-10 sm:mb-0'>
            <div className='flex items-center gap-5 bg-slate-100 rounded-lg w-full sm:w-[40%] justify-between px-2 py-3'>
              <button onClick={decrement} type='button' className='text-primary p-1'>
                <Image src='/images/icon-minus.svg' alt='cart' width={15} height={15} />
              </button>
              <div className='text-xl font-semibold'>{count}</div>
              <button onClick={increment} type='button' className='text-primary p-1'>
                <Image src='/images/icon-plus.svg' alt='cart' width={15} height={15} />
              </button>
            </div>
            <button
              type='button'
              onClick={() => handleAddToCart(count)}
              className='flex items-center justify-center bg-primary w-full sm:w-[60%] py-3 gap-3 text-white rounded-lg hover:shadow-primary hover:opacity-75 shadow-lg transition ease-in duration-200 focus:outline-none focus:bg-opacity-75'>
              <Image src='/images/icon-cart-2.svg' alt='cart' width={20} height={20} />
              Add to cart
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Women

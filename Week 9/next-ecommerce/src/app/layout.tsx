import Navbar from '@/components/Navbar'
import './globals.css'
import type { Metadata } from 'next'
import { Kumbh_Sans } from 'next/font/google'
import { ReduxProvider } from '@/redux/providers'

const kumbh_sans = Kumbh_Sans({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'E-commerce',
  description: 'E-commerce',
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang='en'>
      <body className={`${kumbh_sans.className} dark:bg-slate-900 antialiased`}>
        <ReduxProvider>
          <Navbar />
          {children}
        </ReduxProvider>
      </body>
    </html>
  )
}

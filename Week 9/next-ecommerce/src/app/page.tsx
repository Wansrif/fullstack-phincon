import Image from 'next/image'

const Home = () => {
  return <main className='flex antialiased'>Home</main>
}

export default Home

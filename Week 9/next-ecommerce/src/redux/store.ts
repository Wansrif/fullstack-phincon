import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { persistStore, persistReducer, REHYDRATE, PERSIST, REGISTER } from 'redux-persist'
import storage from '@utils/storage'

import counterReducer from './features/Counter/counterSlice'
import productReducer from './features/Product/productSlice'

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['product'],
}

const rootReducer = combineReducers({
  counter: counterReducer,
  product: productReducer,
  //add all your reducers here
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [REHYDRATE, PERSIST, REGISTER],
      },
    }),
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const persistor = persistStore(store)

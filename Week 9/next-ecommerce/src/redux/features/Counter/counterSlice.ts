import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0,
  },
  reducers: {
    incrementCounter: (state) => {
      state.value += 1
    },
    decrementCounter: (state) => {
      if (state.value > 0) {
        state.value -= 1
      }
    },
    incrementByAmountCounter: (state, action) => {
      state.value += action.payload
    },
  },
})

export const { incrementCounter, decrementCounter, incrementByAmountCounter } = counterSlice.actions

export default counterSlice.reducer

import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { StaticImageData } from 'next/image'

export interface Product {
  id: string
  name: string
  thumbnail: string | StaticImageData
  price: number
  amount: number
}

interface ProductState extends Array<Product> {}

const initialState: ProductState = []

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    addProduct: (state, action: PayloadAction<Product>) => {
      state.push(action.payload)
    },
    removeProduct: (state, action: PayloadAction<string>) => {
      return state.filter((product) => product.id !== action.payload)
    },
  },
})

export const { addProduct, removeProduct } = productSlice.actions

export default productSlice.reducer

'use client'

import { useEffect, useState } from 'react'
import { useAppSelector } from '@/redux/hooks'
import { Outfit } from 'next/font/google'
import Link from 'next/link'
import Image from 'next/image'
import CartModal from '../CartModal'
import NavLink from '../Navlink'

const outfit = Outfit({ subsets: ['latin'] })

const Navbar = () => {
  const [isOpen, setOpen] = useState<Boolean>(false)
  const [cart, setCart] = useState<Array<any>>([])
  const [isShow, setShow] = useState<Boolean>(false)

  const links = [
    {
      label: 'Collection',
      href: '/',
    },
    {
      label: 'Men',
      href: '#',
    },
    {
      label: 'Women',
      href: '/women',
    },
    {
      label: 'About',
      href: '#',
    },
    {
      label: 'Contact',
      href: '#',
    },
  ]

  const handleOpen = () => {
    setOpen(!isOpen)
  }

  const handleShow = () => {
    setShow(!isShow)
  }

  const products = useAppSelector((state) => state.product)

  useEffect(() => {
    setCart(products)
  }, [products])

  return (
    <div className='flex items-center dark:bg-slate-900 sm:px-32 relative'>
      <div className='flex border-b-2 border-slate-200 w-full py-7 px-4 justify-between relative'>
        <div className='flex item-center gap-8'>
          <div className='flex items-center'>
            <Image src='/images/icon-menu.svg' alt='menu' width={18} height={0} onClick={handleShow} className='sm:hidden' />
          </div>
          <Link href='/' className={`${outfit.className} text-3xl font-bold tracking-[0.0775rem]`}>
            sneakers
          </Link>
          <div className='hidden items-center gap-4 sm:flex'>
            {links.map(({ href, label }, index) => (
              <NavLink className='navLink' activeClassName='activeNavLink' key={index} href={href}>
                {label}
              </NavLink>
            ))}
          </div>
        </div>
        {isShow && (
          <div className='fixed w-full bg-black/50 top-0 left-0 z-10'>
            <div className='flex flex-col sm:hidden w-[70%] bg-white min-h-screen'>
              <Image src='/images/icon-close.svg' alt='hamburger' width={15} height={15} className='m-5' onClick={() => setShow(!isShow)} />
              {links.map(({ href, label }, index) => (
                <Link
                  className='text-slate-800 hover:text-amber-500 capitalize px-5 py-2 rounded-md text-lg font-bold transition duration-500 ease-in-out'
                  key={index}
                  href={href}>
                  {label}
                </Link>
              ))}
            </div>
          </div>
        )}
        <div className='flex item-center gap-8'>
          <div className='relative flex item-center'>
            <button type='button' onClick={handleOpen} className='focus:outline-none relative'>
              <Image src='/images/icon-cart.svg' alt='cart' width={20} height={20} />
              {cart.length > 0 && (
                <div className='absolute top-0 -right-2 bg-primary text-white rounded-full w-5 h-5 flex justify-center items-center text-xs'>
                  {cart.length}
                </div>
              )}
            </button>
            {isOpen && <CartModal />}
          </div>
          <button type='button' className='border border-primary rounded-full'>
            <Image src='/images/image-avatar.png' alt='avatar' width={40} height={40} />
          </button>
        </div>
      </div>
    </div>
  )
}

export default Navbar

'use client'

import { NavLinkProps } from '@/types'
import Link from 'next/link'
import { usePathname } from 'next/navigation'

const NavLink = ({ className, activeClassName, ...props }: NavLinkProps) => {
  const pathname = usePathname()
  const isActive = pathname === props.href
  return <Link className={isActive ? activeClassName : className} {...props} />
}

export default NavLink

'use client'

import Image from 'next/image'
import { useAppSelector, useAppDispatch } from '@/redux/hooks'
import { removeProduct } from '@/redux/features/Product/productSlice'

const CartModal = () => {
  const products = useAppSelector((state) => state.product)
  const dispatch = useAppDispatch()

  const handleRemoveProduct = (id: string) => {
    dispatch(removeProduct(id))
  }

  return (
    <div className='shadow-lg rounded-xl absolute z-10 bg-white top-20 sm:top-16 w-96 sm:w-80 sm:-left-48 sm:right-0 -right-[5.35rem]'>
      <div className='flex flex-col'>
        <h3 className='text-lg font-bold px-4 py-3 w-full border-b'>Cart</h3>
        <div className='flex flex-col items-start py-3 px-4 gap-3'>
          <div className='flex flex-col items-center w-full gap-3'>
            {products.length > 0 ? (
              products.map(({ id, name, thumbnail, price, amount }, index) => (
                <div className='flex items-center justify-between w-full gap-2' key={index}>
                  <Image src={thumbnail} alt='product' width={50} height={50} className='rounded-xl' />
                  <div className='flex flex-col'>
                    <div className='text-sm'>{name}</div>
                    <div className='flex items-center gap-1'>
                      <div className='text-sm text-slate-700'>${price}</div>
                      <div className='text-sm'>x {amount}</div>
                      <div className='text-sm text-primary font-bold'>$375.00</div>
                    </div>
                  </div>
                  <Image
                    onClick={() => handleRemoveProduct(id)}
                    src='/images/icon-delete.svg'
                    alt='delete'
                    width={15}
                    height={15}
                    className='cursor-pointer'
                  />
                </div>
              ))
            ) : (
              <div className='text-sm text-slate-700 h-40 flex items-center justify-center'>Your cart is empty</div>
            )}
          </div>
          {products.length > 0 && (
            <button
              type='button'
              className='flex items-center justify-center bg-primary w-full py-3 gap-3 text-white rounded-lg hover:shadow-primary hover:opacity-75 shadow-lg transition ease-in duration-200 focus:outline-none focus:bg-opacity-75'>
              <Image src='/images/icon-cart-2.svg' alt='cart' width={20} height={20} />
              Checkout
            </button>
          )}
        </div>
      </div>
    </div>
  )
}

export default CartModal

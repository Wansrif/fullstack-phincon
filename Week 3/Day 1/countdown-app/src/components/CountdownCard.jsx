import PropTypes from "prop-types";
export default function CountdownCard({ flip, value, children }) {
  const formattedValue = value.toString().padStart(2, "0");

  return (
    <figure className="card-parents bounce">
      <div className="card">
        <p className={`card ${flip ? "flip" : ""}`}>{formattedValue}</p>
        <div className="top"></div>
        <div className="top-bg"></div>
        <div className="middle-line"></div>
        <div className="bottom"></div>
      </div>
      <figcaption>{children}</figcaption>
    </figure>
  );
}

CountdownCard.propTypes = {
  flip: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired,
  children: PropTypes.string.isRequired,
};

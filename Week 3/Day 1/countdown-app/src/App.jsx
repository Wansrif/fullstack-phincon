import "./App.css";
import { useEffect, useState } from "react";
import CountdownCard from "./components/CountdownCard";

function App() {
  // Set the date we're counting down to (in this case, it's Indonesia's Independence Day)
  // use url params to set the date to count down to (in this case, it's "localhost:5173/?date=17-August-2023"")
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [flipDays, setFlipDays] = useState(false);
  const [flipHours, setFlipHours] = useState(false);
  const [flipMinutes, setFlipMinutes] = useState(false);
  const [flipSeconds, setFlipSeconds] = useState(false);

  const url = new URL(window.location.href);
  const date = url.searchParams.get("date");
  const countDownDate = date ? new Date(date).getTime() : new Date("Aug 17, 2023 00:00:00").getTime();

  const countDate = date
    ? new Date(date).toLocaleString("en-GB", {
        year: "numeric",
        month: "long",
        day: "numeric",
      })
    : "17 August 2023";

  useEffect(() => {
    const interval = setInterval(() => {
      // Get today's date and time
      const now = new Date().getTime();

      // Find the distance between now and the count down date
      const distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      const newDays = Math.floor(distance / (1000 * 60 * 60 * 24));
      const newHours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const newMinutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const newSeconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Check if the values have changed and set the flip animation state variables accordingly
      setFlipDays(newDays !== days);
      setFlipHours(newHours !== hours);
      setFlipMinutes(newMinutes !== minutes);

      // Update the state variables
      setDays(newDays);
      setHours(newHours);
      setMinutes(newMinutes);
      setSeconds(newSeconds);

      // Check if seconds have changed and set the flip animation state variable accordingly
      if (newSeconds !== seconds) {
        setFlipSeconds(true);
        setTimeout(() => {
          setFlipSeconds(false);
        }, 500);
      }

      // If the count down is over, write some text
      if (distance < 0) {
        clearInterval(interval);
        setDays(0);
        setHours(0);
        setMinutes(0);
        setSeconds(0);
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [countDownDate, days, hours, minutes, seconds]);

  return (
    <>
      <div className="container">
        <header>WE&apos;RE LAUNCHING SOON</header>

        <div className="title">
          Countdown to <span>{countDate}</span>
        </div>

        <div className="timer">
          <CountdownCard flip={flipDays} value={days}>
            DAYS
          </CountdownCard>

          <CountdownCard flip={flipHours} value={hours}>
            HOURS
          </CountdownCard>

          <CountdownCard flip={flipMinutes} value={minutes}>
            MINUTES
          </CountdownCard>

          <CountdownCard flip={flipSeconds} value={seconds}>
            SECONDS
          </CountdownCard>
        </div>

        <footer>
          <img src="/images/pattern-hills.svg" alt="" />
          <div className="socmed">
            <img src="/images/icon-facebook.svg" alt="" />
            <img src="/images/icon-pinterest.svg" alt="" />
            <img src="/images/icon-instagram.svg" alt="" />
          </div>
        </footer>
      </div>
    </>
  );
}

export default App;

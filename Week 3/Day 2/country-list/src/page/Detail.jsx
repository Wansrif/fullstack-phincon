import axios from 'axios'
import classes from '@/App.module.scss'
import { Box, Button, Skeleton, Typography, styled } from '@mui/material'
import { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'

export default function Detail() {
  const [loading, setLoading] = useState(true)
  const [country, setCountry] = useState([])

  // get country name from url
  const { name: countryName } = useParams()

  // Fetch country detail
  useEffect(() => {
    const fetchCountryDetail = async () => {
      try {
        const res = await axios.get(`https://restcountries.com/v3.1/name/${countryName}?fullText=true`)
        setCountry(res.data)
        setLoading(false)
      } catch (error) {
        console.log(error)
      }
    }
    fetchCountryDetail()
  }, [countryName])

  const CustomBox = styled(Box)(({ theme }) => ({
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    gap: '3rem',
    paddingTop: '2rem',
    [theme.breakpoints.down('sm')]: { flexDirection: 'column', gap: '1rem' },
  }))

  const CustomButton = styled(Button)(({ theme }) => ({
    color: theme.palette.mode === 'dark' ? '#fff' : '#000',
    textTransform: 'none',
    boxShadow: 'rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px',
    backgroundColor: theme.palette.mode === 'dark' ? '#334155' : '#f8fafc',
    padding: '0.5rem 1.5rem',

    '&:hover': {
      backgroundColor: theme.palette.mode === 'dark' ? '#475569' : '#e2e8f0',
      boxShadow: 'rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px',
    },
  }))

  const formatPopulation = (population) => {
    return new Intl.NumberFormat('id').format(population)
  }

  return (
    <main>
      <Link to='/' className={classes.back_link}>
        <CustomButton startIcon={<ArrowBackIcon />}>Back</CustomButton>
      </Link>
      {loading && (
        <CustomBox>
          <Box sx={{ width: '100%' }}>
            <Skeleton animation='wave' variant='rounded' height={400} />
          </Box>
          <Box sx={{ width: '100%' }}>
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '3rem' }} width={'40%'} />
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} />
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} />
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} />
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} width='60%' />
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} width='40%' />
            <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} width='30%' />
          </Box>
        </CustomBox>
      )}
      {country.map((country, i) => (
        <div key={i} className={classes.detail_container}>
          <div className={classes.detail_flag}>
            <img src={country.flags.png} alt={country.name.common} />
          </div>

          <div className={classes.detail_info}>
            <Typography variant='h5' color='text.primary' fontWeight={800}>
              {country.name.common}
            </Typography>

            <div className={classes.detail_info_1}>
              <div className={classes.detail_col_1}>
                <Typography variant='body2' color='text.secondary'>
                  <b>Native Name</b>: {country.name.nativeName ? Object.values(country.name.nativeName)[0].common : 'None'}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  <b>Population</b>: {formatPopulation(country.population)}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  <b>Region</b>: {country.region ? country.region : 'None'}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  <b>Sub Region</b>: {country.subregion ? country.subregion : 'None'}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  <b>Capital</b>: {country.capital ? country.capital : 'None'}
                </Typography>
              </div>
              <div className={classes.detail_col_2}>
                <Typography variant='body2' color='text.secondary'>
                  <b>Top Level Domain</b>: {country.tld}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  <b>Currencies</b>:
                  {country.currencies?.length > 0 ? country.currencies.map((currency, i) => <span key={i}>{currency.name}</span>) : <span>None</span>}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  <b>Languages</b>:
                  {country.languages &&
                    Object.values(country.languages).map((language, i, languagesArray) => (
                      <span key={i}>
                        {language}
                        {i < languagesArray.length - 1 && ', '}
                      </span>
                    ))}
                </Typography>
              </div>
            </div>
            <div className={classes.detail_info_2}>
              <Typography variant='body2' color='text.secondary'>
                <b>Border Countries</b>:
                {country?.borders && country?.borders.length > 0 ? (
                  <span className={classes.borders}>
                    {country.borders.map((border) => (
                      <CustomButton key={border} variant='contained' size='small'>
                        {border}
                      </CustomButton>
                    ))}
                  </span>
                ) : (
                  'None'
                )}
              </Typography>
            </div>
          </div>
        </div>
      ))}
    </main>
  )
}

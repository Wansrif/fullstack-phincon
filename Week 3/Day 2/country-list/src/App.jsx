import axios from 'axios'
import { useEffect, useMemo, useState } from 'react'
import classes from '@/App.module.scss'
import SearchBar from '@/components/search/SearchBar'
import FilterMenu from '@/components/filter/FilterMenu'
import CardCountries from '@/components/card/CardCountries'

import Grid from '@mui/material/Grid'
import Skeleton from '@mui/material/Skeleton'
import { Box, Pagination, PaginationItem } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import usePagination from './services/Pagination'
import { Link } from 'react-router-dom'
import Sorting from '@/components/sort/Sorting'

function App() {
  const [countries, setCountries] = useState([])
  const [loading, setLoading] = useState(true)
  const [search, setSearch] = useState('')
  const [region, setRegion] = useState('')
  const [sortType, setSortType] = useState('ascending')

  // Fetch countries
  useEffect(() => {
    const fetchCountries = async () => {
      try {
        // const res = await axios.get("https://restcountries.com/v3.1/all?fields=name,flags,population,region,capital");
        const res = await axios.get('https://restcountries.com/v3.1/all')
        setCountries(res.data)
        setLoading(false)
      } catch (error) {
        console.log(error)
      }
    }
    fetchCountries()
  }, [])

  // Handle search
  const handleSearchChange = (event) => {
    setSearch(event.target.value)
  }

  const sortedAndFilteredCountries = useMemo(() => {
    let filteredCountries = countries

    if (region) {
      filteredCountries = filteredCountries.filter((country) => country.region === region)
    }

    filteredCountries = filteredCountries.filter((country) => country.name.common.toLowerCase().includes(search.toLowerCase()))

    if (sortType === 'ascending') {
      return [...filteredCountries].sort((a, b) => a.name.common.localeCompare(b.name.common))
    }

    if (sortType === 'descending') {
      return [...filteredCountries].sort((a, b) => b.name.common.localeCompare(a.name.common))
    }

    return filteredCountries
  }, [countries, region, search, sortType])

  // Pagination
  const itemsPerPage = 8
  const { currentData, paginate, currentPage, maxPage } = usePagination(sortedAndFilteredCountries, itemsPerPage)

  const handlePageChange = (event, value) => {
    paginate(value)
  }

  return (
    <>
      <main>
        <header>
          <SearchBar value={search} onChange={handleSearchChange} />
          <div className={classes.filter}>
            <FilterMenu setRegion={setRegion} />
            <Sorting sortType={sortType} setSortType={setSortType} />
          </div>
        </header>
        <div className={classes.container}>
          <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, sm: 8, md: 12 }}>
            {loading ? (
              Array.from({ length: 8 }).map((_, index) => (
                <Grid item xs={2} sm={4} md={3} key={index}>
                  <Box sx={{ width: '100%' }}>
                    <Skeleton animation='wave' variant='rounded' height={140} />
                    <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} />
                    <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} width='60%' />
                    <Skeleton animation='wave' variant='text' sx={{ fontSize: '1rem' }} width='60%' />
                  </Box>
                </Grid>
              ))
            ) : sortedAndFilteredCountries.length > 0 ? (
              currentData.map((country, i) => (
                <Grid item xs={2} sm={4} md={3} key={i}>
                  <Link to={`/detail/${country.name.common.toLowerCase()}`} className={classes.link}>
                    <CardCountries
                      flag={country.flags.png}
                      name={country.name.common}
                      population={country.population}
                      region={country.region}
                      capital={country.capital}
                    />
                  </Link>
                </Grid>
              ))
            ) : (
              <Grid item xs={12} sm={12} md={12}>
                <h1 className={classes.notFound}>No results found</h1>
              </Grid>
            )}
          </Grid>

          {/* Pagination */}
          {loading ? null : sortedAndFilteredCountries.length > 0 ? (
            <Box sx={{ display: 'flex', justifyContent: 'center', mt: 5 }}>
              <Pagination
                color='primary'
                count={maxPage}
                page={currentPage}
                onChange={handlePageChange}
                boundaryCount={2}
                renderItem={(item) => <PaginationItem slots={{ previous: ArrowBackIcon, next: ArrowForwardIcon }} {...item} />}
              />
            </Box>
          ) : null}
        </div>
      </main>
    </>
  )
}

export default App

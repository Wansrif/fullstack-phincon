import Navbar from '@/components/navbar/Navbar'
import { ThemeProvider } from '@emotion/react'
import { CssBaseline, createTheme, responsiveFontSizes, useMediaQuery } from '@mui/material'
import { useEffect, useMemo, useState } from 'react'
import { Outlet } from 'react-router-dom'

export default function Root() {
  // Dark mode
  const storedDarkMode = JSON.parse(localStorage.getItem('darkMode'))
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')
  const [darkMode, setDarkMode] = useState(storedDarkMode !== null ? storedDarkMode : prefersDarkMode)

  // Theme
  const theme = useMemo(() => {
    const mainPalette = {
      primary: {
        main: '#6366f1',
      },
      background: {
        default: '#f1f5f9',
      },
    }

    const darkModePalette = {
      primary: {
        main: '#4f46e5',
      },
      background: {
        default: '#1e293b',
      },
    }

    return createTheme({
      palette: {
        mode: darkMode ? 'dark' : 'light',
        ...mainPalette,
        ...(darkMode ? darkModePalette : {}),
      },
    })
  }, [darkMode])

  // Responsive fonts
  const themeWithResponsiveFonts = responsiveFontSizes(theme)

  // Save dark mode to local storage
  useEffect(() => {
    localStorage.setItem('darkMode', JSON.stringify(darkMode))
  }, [darkMode])

  // Handle dark mode
  const handleDarkMode = () => {
    setDarkMode(!darkMode)
  }

  return (
    <>
      <ThemeProvider theme={themeWithResponsiveFonts}>
        <CssBaseline />
        <Navbar darkMode={darkMode} handleDarkMode={handleDarkMode} />
        <Outlet />
      </ThemeProvider>
    </>
  )
}

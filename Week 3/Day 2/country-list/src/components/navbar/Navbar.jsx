import classes from '@/components/navbar/Navbar.module.scss'
import { Button, Link, styled } from '@mui/material'
import { BsMoonStars, BsSun } from 'react-icons/bs'
import { grey } from '@mui/material/colors'

export default function Navbar({ darkMode, handleDarkMode }) {
  const ThemeButton = styled(Button)(() => ({
    color: darkMode ? grey[300] : '#f8fafc',
    padding: '0.5rem 1rem',
    backgroundColor: darkMode ? '#475569' : '#818cf8',
    textTransform: 'capitalize',
    borderRadius: '0.5rem',
    '&:hover': {
      backgroundColor: darkMode ? '#1e293b' : '#4f46e5',
    },
  }))

  const Logo = styled(Link)(() => ({
    color: darkMode ? grey[300] : grey[200],
    fontWeight: 'bold',
    fontSize: '1.5rem',
    '&:hover': {
      color: darkMode ? grey[100] : grey[100],
    },
    '@media (max-width: 486px)': {
      fontSize: '1rem',
    },
  }))
  return (
    <div className={`${classes.navbar} ${darkMode ? classes.dark : ''}`}>
      <Logo href='/' underline='none'>
        Where in the world?
      </Logo>
      <ThemeButton onClick={handleDarkMode} startIcon={darkMode ? <BsMoonStars /> : <BsSun />}>
        {darkMode ? 'Dark Mode' : 'Light Mode'}
      </ThemeButton>
    </div>
  )
}

import { useState } from "react";

import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { grey } from "@mui/material/colors";

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color: theme.palette.mode === "light" ? "rgb(55, 65, 81)" : theme.palette.grey[300],
    backgroundColor: theme.palette.mode === "light" ? "#fafafa" : "#334155",
    boxShadow:
      "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(theme.palette.primary.main, theme.palette.action.selectedOpacity),
      },
    },
  },
}));

const CustomButton = styled(Button)(({ theme }) => ({
  color: theme.palette.mode === "dark" ? grey[300] : "#4f46e5",
  textTransform: "none",
  boxShadow: "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
  backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#f8fafc",
  padding: "0.5rem 1.5rem",
  width: "100%",
  borderRadius: "0.5rem",
  display: "flex",
  justifyContent: "space-between",

  [theme.breakpoints.down("sm")]: {
    padding: "0.5rem 1rem",
  },

  "&:hover": {
    backgroundColor: theme.palette.mode === "dark" ? "#475569" : "#e2e8f0",
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
  },

  "@media (min-width: 768px)": {
    width: "auto",
  },
}));

export default function FilterMenu({ setRegion }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleRegionChange = (event) => {
    if (event.target.textContent === "All") setRegion("");
    else setRegion(event.target.textContent);
    handleClose();
  };

  return (
    <>
      <CustomButton
        id="demo-customized-button"
        aria-controls={open ? "demo-customized-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        variant="outline"
        disableElevation
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
      >
        Filter by Region
      </CustomButton>
      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          "aria-labelledby": "demo-customized-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleRegionChange} disableRipple>
          All
        </MenuItem>
        <MenuItem onClick={handleRegionChange} disableRipple>
          Africa
        </MenuItem>
        <MenuItem onClick={handleRegionChange} disableRipple>
          Americas
        </MenuItem>
        <MenuItem onClick={handleRegionChange} disableRipple>
          Asia
        </MenuItem>
        <MenuItem onClick={handleRegionChange} disableRipple>
          Europe
        </MenuItem>
        <MenuItem onClick={handleRegionChange} disableRipple>
          Oceania
        </MenuItem>
      </StyledMenu>
    </>
  );
}

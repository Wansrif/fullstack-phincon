import classes from "./CardCountry.module.scss";

export default function CardRegion({ flag, name, population, region, capital }) {
  return (
    <div className={classes.card}>
      <img src={flag} alt={name} className={classes.card_flag} />
      <div className={classes.card_title}>
        <p>{name}</p>
      </div>
      <div className={classes.card_info}>
        <p>
          <span>Population:</span> {population}
        </p>
        <p>
          <span>Region:</span> {region}
        </p>
        <p>
          <span>Capital:</span> {capital}
        </p>
      </div>
    </div>
  );
}

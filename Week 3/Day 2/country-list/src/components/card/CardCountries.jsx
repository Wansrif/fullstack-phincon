import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea, styled } from "@mui/material";

export default function CardCountries({ flag, name, population, region, capital }) {
  const CustomCard = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#fff",
    color: theme.palette.mode === "dark" ? "#fff" : "#000",
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
    borderRadius: "0.5rem",
    "&:hover": {
      backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#fff",
      color: theme.palette.mode === "dark" ? "#fff" : "#000",
    },
  }));

  // formated population
  population = new Intl.NumberFormat("id").format(population);

  return (
    <CustomCard>
      <CardActionArea>
        <CardMedia component="img" height="140" src={flag} alt={name} sx={{ objectFit: "contain" }} />
        <CardContent sx={{ minHeight: 160 }}>
          <Typography gutterBottom variant="h5" component="div">
            {name}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <b>Population:</b> {population}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <b>Region:</b> {region}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <b>Capital:</b> {capital}
          </Typography>
        </CardContent>
      </CardActionArea>
    </CustomCard>
  );
}

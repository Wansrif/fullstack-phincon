import { styled } from "@mui/material";
import classes from "./SearchBar.module.scss";
import { BiSearch } from "react-icons/bi";

const CustomInput = styled("input")(({ theme }) => ({
  color: theme.palette.mode === "dark" ? "#fff" : "#111827",
  backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#f3f4f6",
  padding: "0.75rem 1.5rem 0.75rem 3rem",
  width: "100%",
  borderRadius: "0.5rem",
  border: theme.palette.mode === "dark" ? "2px solid #334155" : "2px solid #e5e7eb",
  outline: "none",
  transition: "all 0.3s ease",

  "&::placeholder": {
    color: theme.palette.mode === "dark" ? "#94a3b8" : "#94a3b8",
  },

  "&:focus": {
    outline: "none",
    borderColor: "#6366f1",
    backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#fff",
    boxShadow: "0 0 0 4px rgba(131, 62, 235, 0.1)",
  },

  "&:hover": {
    outline: "none",
    borderColor: "#6366f1",
    backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#fff",
    boxShadow: "0 0 0 4px rgba(131, 62, 235, 0.1)",
  },
}));

export default function SearchBar({ value, onChange }) {
  return (
    <div className={classes.search_bar}>
      <BiSearch className={classes.BiSearch} />
      <CustomInput type="text" placeholder="Search for a country..." value={value} onChange={onChange} />
    </div>
  );
}

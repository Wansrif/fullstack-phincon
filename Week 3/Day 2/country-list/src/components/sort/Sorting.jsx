import { Button, styled } from "@mui/material";
import { grey } from "@mui/material/colors";
import { BsSortAlphaDown, BsSortAlphaUpAlt } from "react-icons/bs";

export default function Sorting({ sortType, setSortType }) {
  const SortingButton = styled(Button)(({ theme }) => ({
    color: theme.palette.mode === "dark" ? grey[300] : "#4f46e5",
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
    backgroundColor: theme.palette.mode === "dark" ? "#334155" : "#f8fafc",
    padding: "0.5rem 1.5rem",
    width: "100%",
    borderRadius: "0.5rem",

    "&:hover": {
      backgroundColor: theme.palette.mode === "dark" ? "#475569" : "#e2e8f0",
      boxShadow: "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
    },

    "@media (min-width: 768px)": {
      width: "auto",
    },
  }));

  const handleSortToggle = () => {
    setSortType((prevSortType) => (prevSortType === "ascending" ? "descending" : "ascending"));
  };
  return (
    <>
      <SortingButton onClick={handleSortToggle}>
        {sortType === "ascending" ? <BsSortAlphaDown size={"1.5rem"} /> : <BsSortAlphaUpAlt size={"1.5rem"} />}
      </SortingButton>
    </>
  );
}
